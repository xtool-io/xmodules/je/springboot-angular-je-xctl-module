import {inject} from '@angular/core';
import {ActivatedRouteSnapshot, ResolveFn, RouterStateSnapshot} from '@angular/router';
import {lastValueFrom, Observable, of} from 'rxjs';
import DataSource from "devextreme/data/data_source";
import {${entity.name}Service} from "app/service/${entity.resourceName}.service";
import CustomStore from "devextreme/data/custom_store";


export const ${entity.name}DataSourceResolver: ResolveFn<DataSource> = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot,
  service: ${entity.name}Service = inject(${entity.name}Service)
): Observable<DataSource> => {
  const dataSource = new DataSource({
    store: new CustomStore({
      key: 'id',
      byKey: (key: number) => lastValueFrom(service.findById(key)),
      load: options => lastValueFrom(service.findAll(options))
    }),
  })
  return of(dataSource);
}

