import {inject} from '@angular/core';
import {ActivatedRouteSnapshot, ResolveFn, Router, RouterStateSnapshot} from '@angular/router';
import {lastValueFrom, Observable, of} from 'rxjs';
import { ${entity.name}Service } from 'app/service/${entity.resourceName}.service';
import {catchError, take} from "rxjs/operators";
import {${entity.name}FormData} from "../domain/${entity.resourceName}-form-data";

export const ${entity.name}FormDataResolver: ResolveFn<${entity.name}FormData> = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot,
  service: ${entity.name}Service = inject(${entity.name}Service),
  router: Router = inject(Router)
): Observable<${entity.name}FormData> => {
  const id = route.params['id'];
  if (id && !isNaN(id)) {
    return service.findById(id).pipe(
      take(1),
      catchError(_ => {
        return router.navigate(['/${entity.resourceName}']);
      })
    );
  }
  return of(new ${entity.name}FormData());
}
