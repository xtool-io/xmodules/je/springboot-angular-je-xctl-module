[#ftl]
[#-- @implicitly included --]
[#-- @ftlvariable name="rel" type="xtool.xdepplantuml.model.PlantRelationship" --]
[#-- @ftlvariable name="plantClass" type="xtool.xdepplantuml.model.PlantClass" --]
[#-- @ftlvariable name="pAttr" type="xtool.xdepplantuml.model.PlantField" --]
[#-- @ftlvariable name="attr" type="xtool.xdepplantuml.model.PlantField" --]
[#-- @ftlvariable name="jpaAttr" type="xtool.xdepspringboot.model.EntityAttribute" --]
[#-- @ftlvariable name="plantTask" type="xtool.xdepplantuml.tasks.PlantTask" --]
[#-- @ftlvariable name="entity" type="xtool.xdepspringboot.model.Entity" --]
[#-- @ftlvariable name="listViewEntity" type="xtool.xdepspringboot.model.Entity" --]
