<dxi-item itemType="group" caption="Selecione um ${toOneEntitie.name}">
    <dxi-item>
        <dxo-label text="${toOneEntitie.name}"></dxo-label>
        <div *dxTemplate>
            <dx-select-box
                    displayExpr="nome"
                    valueExpr="id"
                    [readOnly]="!isEditing"
                    [(value)]="formData.${toOneEntitie.name?uncap_first}"
                    [dataSource]="${toOneEntitie.name?uncap_first}DataSource">
            </dx-select-box>
        </div>
    </dxi-item>
</dxi-item>
