<dxi-item itemType="group" caption="Selecione um ${toManyEntitie.name}">
    <dxi-item>
        <dxo-label text="${toManyEntitie.name}"></dxo-label>
        <div *dxTemplate>
            <dx-tag-box
                    [readOnly]="!isEditing"
                    valueExpr="id"
                    displayExpr="nome"
                    [(value)]="formData.${toManyEntitie.name?uncap_first}"
                    [dataSource]="${toManyEntitie.name?uncap_first}DataSource">
            </dx-tag-box>
        </div>
    </dxi-item>
</dxi-item>

