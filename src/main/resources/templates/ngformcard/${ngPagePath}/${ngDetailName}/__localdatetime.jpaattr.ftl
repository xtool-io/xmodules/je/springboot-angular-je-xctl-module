<#if jpaAttr.type.name == 'LocalDateTime'>

        <!--FIELD ${jpaAttr.name?upper_case} -->
        <dxi-item>
            <dxo-label text="${jpaAttr.name}"></dxo-label>
            <div *dxTemplate>
                <dx-date-box
                    [readOnly]="!isEditing"
                    [(value)]="formData.${jpaAttr.name}"
                    type="datetime"
                    displayFormat="dd/MM/yyyy hh:mm"
                    useMaskBehavior="true"
                    openOnFieldClick="true">

                    <!--VALIDATION-->
                    <dx-validator>
                        <#if jpaAttr.nullable>
                            <!--REQUIRED-->
                            <dxi-validation-rule
                                    type="required"
                                    message="Este campo é obrigatório">
                            </dxi-validation-rule>
                        </#if>
                    </dx-validator>
                </dx-date-box>
            </div>
        </dxi-item>
</#if>

