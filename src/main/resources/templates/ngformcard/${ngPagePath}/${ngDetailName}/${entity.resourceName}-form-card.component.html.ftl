<#assign basicAttributes=entity.attributes?filter(attr -> attr.hasAnnotation('Column'))>
<dx-scroll-view useNative="true">
  <div id="form-${entity.resourceName}" class="form-block content-block">
    <dx-validation-group>
      <div class="dx-card responsive-paddings">
        <dx-toolbar>
          <dxi-item location="before">
            <dx-button
                    icon="arrowleft"
                    (onClick)="back()">
            </dx-button>
          </dxi-item>
          <dxi-item location="before"><span class="form-title">{{ formData.id ? 'Edição' : 'Criação' }} de ${entity.name}</span>
          </dxi-item>
          <dxi-item
                  location="after"
                  locateInMenu="after"
                  [visible]="!isEditing">
            <div *dxTemplate>
              <dx-button
                      *ngxPermissionsOnly="['${entity.resourceName}:resource:update']"
                      text="Editar"
                      type="normal"
                      stylingMode="text"
                      (onClick)="edit()"
                      [visible]="!!formData.id"
              >
              </dx-button>
            </div>
          </dxi-item>
          <dxi-item
                  location="after"
                  locateInMenu="after"
                  [visible]="!isEditing">
            <div *dxTemplate>
              <dx-button
                      *ngxPermissionsOnly="['${entity.resourceName}:resource:delete']"
                      text="Excluir"
                      type="danger"
                      stylingMode="text"
                      (onClick)="delete()"
                      [visible]="!!formData.id">
              </dx-button>
            </div>
          </dxi-item>
        </dx-toolbar>

        <dx-form
                class="form last plain-styled-form"
                id="form"
                [formData]="formData"
                [class.view-mode]="!isEditing"
                requiredMark="*">


          <#list basicAttributes as jpaAttr>
            <#include '__localdatetime.jpaattr.ftl'>
            <#include '__localdate.jpaattr.ftl'>
            <#include '__string.jpaattr.ftl'>
            <#include '__boolean.jpaattr.ftl'>
            <#include '__number.jpaattr.ftl'>
          </#list>

          <#list entity.toOneEntities as toOneEntitie>
            <#include '__toone.jpaattr.ftl'>
          </#list>

          <#list entity.toManyEntities as toManyEntitie>
            <#include '__tomany.jpaattr.ftl'>
          </#list>
        </dx-form>

        <dx-toolbar *ngIf="isEditing">
          <dxi-item
                  location="before"
                  locateInMenu="never">
            <dx-button
                    text="Salvar"
                    type="default"
                    (onClick)="save($event)">
            </dx-button>
          </dxi-item>
          <dxi-item
                  location="before"
                  locateInMenu="never">
            <dx-button
                    text="Cancelar"
                    stylingMode="text"
                    (onClick)="cancel()">
            </dx-button>
          </dxi-item>
        </dx-toolbar>
      </div>
    </dx-validation-group>
  </div>
</dx-scroll-view>
<#list entity.toOneEntities as toOneEntity>
  <#if toOneEntity.hasAnnotation("SettingEntity")>
<dx-speed-dial-action
        [visible]="isEditing"
        icon="add"
        label="Novo Tipo de ${toOneEntity.name?cap_first}"
        (onClick)="create${toOneEntity.name?cap_first}()">
</dx-speed-dial-action>
  </#if>
</#list>
