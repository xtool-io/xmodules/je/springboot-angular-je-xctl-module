<#assign basicAttributes=entity.attributes?filter(attr -> attr.hasAnnotation('Column'))>
import {Component} from "@angular/core";
import {${entity.name}FormData} from "app/domain/${entity.resourceName}-form-data";
import {${entity.name}Service} from "app/service/${entity.resourceName}.service";
import notify from "devextreme/ui/notify";
import {take} from "rxjs/operators";
import {lastValueFrom} from 'rxjs';
import {confirm} from "devextreme/ui/dialog";
import {Location} from "@angular/common";
import {ActivatedRoute} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
<#list entity.toOneEntities as toOneEntity>
    <#if toOneEntity.hasAnnotation("SettingEntity")>
import { ${toOneEntity.name}FormDialogComponent } from "../../@settings/${toOneEntity.resourceName}-page/${toOneEntity.resourceName}-detail/${toOneEntity.resourceName}-form-dialog.component";
import { ${toOneEntity.name}FormData } from "../../../domain/${toOneEntity.resourceName}-form-data";
    </#if>
</#list>

@Component({
  selector: 'app-${entity.resourceName}-detail',
  templateUrl: './${entity.resourceName}-form-card.component.html',
  styles: [``]
})

export class ${entity.name}FormCardComponent {

    isEditing: boolean = false;

    formData: ${entity.name}FormData;

    private initialFormData: ${entity.name}FormData;

<#list entity.toOneEntities as toOneEntity>
    ${toOneEntity.name?uncap_first}DataSource: any;
</#list>
<#list entity.toManyEntities as toManyEntity>
    ${toManyEntity.name?uncap_first}DataSource: any;
</#list>

    constructor(
      private dialog: MatDialog,
      private location: Location,
      private activatedRoute: ActivatedRoute,
      private ${entity.name?uncap_first}Service: ${entity.name}Service,
    ) {
        this.formData = this.initialFormData = {...this.activatedRoute.snapshot.data['formData']};
        <#list entity.toOneEntities as toOneEntity>
        this.${toOneEntity.name?uncap_first}DataSource = this.activatedRoute.snapshot.data['${toOneEntity.name?uncap_first}DataSource'];
        </#list>
        <#list entity.toManyEntities as toManyEntity>
        this.${toManyEntity.name?uncap_first}DataSource = this.activatedRoute.snapshot.data['${toManyEntity.name?uncap_first}DataSource'];
        </#list>
        this.isEditing = !this.formData['id']
    }

    back() {
      this.location.back();
    }

    edit() {
      this.isEditing = true;
    }

    delete() {
      confirm(
      'Clique em sim para confirmar a exclusão do Ponto de Transmissão',
      'Aviso')
      .then(result => {
        if (result) {
          this.${entity.name?uncap_first}Service.delete(this.formData['id'])
          .pipe(take(1))
          .subscribe(() => {
            notify('', 'success', 3000);
            this.back()
          });
        }
      });
    }

    save(e) {
        if (!e.validationGroup.validate().isValid) {
            notify("A operação de salvar não foi realizada. Verifique os erros de validação do formulário.", 'error', 3000)
            return;
        }
        const message = this.formData['id'] ? 'Ponto de transmissão criado com sucesso!' : 'Ponto de transmissão atualizado com sucesso!';
        return this.${entity.name?uncap_first}Service.save(this.formData)
        .pipe(take(1))
        .subscribe(resource => {
                notify(
                    {message},
                    'success',
                    5000);
                this.isEditing = false;
                this.formData = this.initialFormData = {...resource}
            }
        );
    }

    cancel() {
        if (!this.formData['id']) this.back();
        this.formData = {...this.initialFormData};
        this.isEditing = false;
    }

    <#list basicAttributes as jpaAttr>
    <#if jpaAttr.unique>
      ${jpaAttr.name}UniqueValidation = (params): Promise<void> => {
        return new Promise<void>((resolve, reject) => {
            lastValueFrom(this.${entity.name?uncap_first}Service.findByExample({${jpaAttr.name}: params.value}))
            .then( (value)  => value.id ? reject() : resolve())
            .catch(error => {
              reject();
              throw new Error(error.message);
            });
          });
         };
    </#if>
    </#list>

    <#list entity.toOneEntities as toOneEntity>
        <#if toOneEntity.hasAnnotation("SettingEntity")>
        create${toOneEntity.name?cap_first}() {
            const dialogRef = this.dialog.open(${toOneEntity.name?cap_first}FormDialogComponent, {
                data: {},
                width: '800px'
            });
        <#list entity.toOneEntities as toOneEntity>
            // dialogRef.componentInstance.${toOneEntity.name?uncap_first}DataSource = this.${toOneEntity.name?uncap_first}DataSource;
        </#list>
        <#list entity.toManyEntities as toManyEntity>
            // dialogRef.componentInstance.${toManyEntity.name?uncap_first}DataSource = this.${toManyEntity.name?uncap_first}DataSource;
        </#list>
        dialogRef.afterClosed().subscribe((result: ${toOneEntity.name?cap_first}FormData) => {
            if (result) {
            this.formData.${toOneEntity.name?uncap_first} = result.id;
                }
            });
        }
        </#if>
    </#list>
}
