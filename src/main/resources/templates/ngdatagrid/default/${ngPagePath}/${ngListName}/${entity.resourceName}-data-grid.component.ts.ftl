import {Component, ViewChild} from '@angular/core';
import {DxDataGridComponent} from "devextreme-angular/ui/data-grid";
import {${entity.name}Service} from "app/service/${entity.resourceName}.service";
import {DataExportType} from 'app/@core/dataexport/data-export-type.enum';
import {ActivatedRoute, Router} from "@angular/router";
import {confirm} from "devextreme/ui/dialog";
import DataSource from "devextreme/data/data_source";

@Component({
  selector: 'app-${entity.resourceName}-data-grid',
  templateUrl: './${entity.resourceName}-data-grid.component.html',
  styles: []
})
export class ${entity.name}DataGridComponent {

  /**
   * Instância do componente DataGrid
   * @see https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/
   */
  @ViewChild(DxDataGridComponent, {static: true})
  readonly dataGrid: DxDataGridComponent;

  /**
   * DataSource do componente DataGrid
   * @see https://js.devexpress.com/Documentation/ApiReference/Data_Layer/DataSource/
   */
  dataSource: DataSource;

  /**
   * True caso o DataGrid esteja com algum filtro aplicado.
   */
  filtered = false;

  /**
   * Total de registros do DataGrid. O número pode variar conforme os filtros aplicados.
   */
  totalResources = 0;

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    private ${entity.name?uncap_first}Service: ${entity.name}Service) {
    // Retorna o dataSource do resolver
    this.dataSource = this.activatedRoute.snapshot.data['dataSource']
    this.dataSource.on({
      'changed': () => this.totalResources = this.dataSource.totalCount()
    });
  }

  <#if !entity.readOnly>
  create() {
    this.router.navigate(['/${entity.resourceName}', 'new']);
  }
  </#if>  

  detail(id: number) {
    this.router.navigate(['/${entity.resourceName}', 'detail', id]);
  }

  refresh() {
    this.dataSource.reload()
  }

  exportCsv() {
    confirm(
      `Deseja exportar os dados de ${entity.name}? <br><b>${r'${this.totalResources}'} registros serão exportado(s).</b>`,
      'Confirme a exportação'
    ).then(result => {
      if (result) {
        this.${entity.name?uncap_first}Service.export(
          {
            sort: this.dataGrid.instance.getDataSource().loadOptions().sort,
            filter: this.dataGrid.instance.getCombinedFilter()
          },
          {
            exportType: DataExportType.CSV,
            columns: this.dataGrid.instance.getVisibleColumns()
              .filter(i => i["dataField"])
              .map(i => i["dataField"])
          }
        )
      }
    });
  }

  /**
   * @see https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onContentReady
   */
  async onContentReady() {
    this.filtered = (
      this.dataSource.isLoaded() &&
      this.dataGrid.instance.getCombinedFilter() &&
      this.dataGrid.instance.getCombinedFilter().length > 0
    ) || false;
  }

  /**
   * Limpa todos os filtros aplicados ao DataGrid
   * @see https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Methods/#clearFilterfilterName
   */
  clearFilters() {
    this.dataGrid.instance.clearFilter('header');
    this.dataGrid.instance.clearFilter('row');
    this.dataGrid.instance.clearFilter('search');
  }

}
