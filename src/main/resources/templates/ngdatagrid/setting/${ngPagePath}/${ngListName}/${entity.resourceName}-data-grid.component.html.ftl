<dx-data-grid
  style="height: 100%"
  [dataSource]="dataSource"
  [allowColumnReordering]="true"
  [allowColumnResizing]="true"
  [columnAutoWidth]="true"
  [wordWrapEnabled]="true"
  [cellHintEnabled]="true"
  [remoteOperations]="true"
  (onContentReady)="onContentReady()">
  <dxo-scrolling mode="virtual" useNative="true"></dxo-scrolling>
  <dxo-sorting mode="multiple"></dxo-sorting>
  <dxo-paging [pageSize]="20"></dxo-paging>
  <dxo-pager [allowedPageSizes]="[20]" [showNavigationButtons]="true" [showInfo]="true" [visible]="this.totalResources > 20"> </dxo-pager>
  <dxo-filter-row [visible]="true"></dxo-filter-row>
  <dxo-header-filter [visible]="true"></dxo-header-filter>
  <dxo-search-panel [visible]="true" [width]="300"></dxo-search-panel>

  <!-- TOOLBAR !-->
  <dxo-toolbar>
    <dxi-item location="before">
      <h5>${entity.name} ({{totalResources}})</h5>
    </dxi-item>

    <#if !entity.readOnly>
    <dxi-item location="before">
      <dx-button
        stylingMode="contained"
        text="Novo"
        hint="novo"
        type="default"
        (onClick)="create()">
      </dx-button>
    </dxi-item>
    </#if>
    <dxi-item location="after">
      <dx-button
        stylingMode="contained"
        text="Limpar Filtro"
        hint="Limpar Filtro"
        [visible]="filtered"
        (onClick)="clearFilters()"
        type="danger">
      </dx-button>
    </dxi-item>

    <dxi-item location="after">
       <dx-button
        stylingMode="contained"
        icon="refresh"
        hint="Atualizar dados"
        (onClick)="refresh()">
      </dx-button>
      <dx-button
        [disabled]="totalResources == 0"
        stylingMode="contained"
        icon="export"
        hint="Exportar dados em formato CSV"
        (onClick)="exportCsv()">
      </dx-button>
    </dxi-item>

    <dxi-item name="searchPanel" locateInMenu="auto"></dxi-item>

  </dxo-toolbar>
  <!-- COLUMNS -->
  <#list listViewEntity.attributes as attr>
  <dxi-column dataField="${attr.name}"
              caption="${attr.name}"
              [allowSearch]="false"
              [allowFiltering]="false"
              [allowHeaderFiltering]="false">
  </dxi-column>
  </#list>

  <!-- BUTTONS -->
     <dxi-column
        type="buttons"
        cellTemplate="buttonsTemplate">
      <div
        *dxTemplate="let data of 'buttonsTemplate'"
        fxLayout="row"
        fxLayoutAlign="space-between center">
        <dx-button
          icon="edit"
          hint="Editar"
          (onClick)="edit(data.data.id)">
        </dx-button>
        <dx-button
          icon="trash"
          hint="Excluir"
          (onClick)="delete(data.data.id)">
        </dx-button>
      </div>
    </dxi-column>
<#--  <dxi-column-->
<#--    type="buttons"-->
<#--    cellTemplate="buttonsTemplate">-->
<#--    <div-->
<#--      *dxTemplate="let data of 'buttonsTemplate'"-->
<#--      fxLayout="row"-->
<#--      fxLayoutAlign="space-between center">-->
<#--      <dx-button-->
<#--        icon="edit"-->
<#--        hint="Editar Eleição"-->
<#--        (onClick)="edit(data.data.id)">-->
<#--      </dx-button>-->
<#--    </div>-->
<#--  </dxi-column>-->
</dx-data-grid>
