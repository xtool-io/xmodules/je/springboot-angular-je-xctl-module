import {Component, ViewChild} from '@angular/core';
import {DxDataGridComponent} from "devextreme-angular/ui/data-grid";
import {${entity.name}Service} from "app/service/${entity.resourceName}.service";
import {DataExportType} from 'app/@core/dataexport/data-export-type.enum';
import {ActivatedRoute, Router} from "@angular/router";
import {confirm} from "devextreme/ui/dialog";
import DataSource from "devextreme/data/data_source";
import {MatDialog} from "@angular/material/dialog";
import {${entity.name}FormData} from "../../../../domain/${entity.resourceName}-form-data";
import {take} from "rxjs/operators";
import {${entity.name}FormDialogComponent} from "../${entity.resourceName}-detail/${entity.resourceName}-form-dialog.component";
import notify from "devextreme/ui/notify";

@Component({
  selector: 'app-${entity.resourceName}-data-grid',
  templateUrl: './${entity.resourceName}-data-grid.component.html',
  styles: []
})
export class ${entity.name}DataGridComponent {

  /**
   * Instância do componente DataGrid
   * @see https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/
   */
  @ViewChild(DxDataGridComponent, {static: true})
  readonly dataGrid: DxDataGridComponent;

  /**
   * DataSource do componente DataGrid
   * @see https://js.devexpress.com/Documentation/ApiReference/Data_Layer/DataSource/
   */
  dataSource: DataSource;

<#list entity.toOneEntities as toOneEntity>
  ${toOneEntity.name?uncap_first}DataSource: any;
</#list>
<#list entity.toManyEntities as toManyEntity>
  ${toManyEntity.name?uncap_first}DataSource: any;
</#list>

  /**
   * True caso o DataGrid esteja com algum filtro aplicado.
   */
  filtered = false;

  /**
   * Total de registros do DataGrid. O número pode variar conforme os filtros aplicados.
   */
  totalResources = 0;

  constructor(
    private dialog: MatDialog,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    private ${entity.name?uncap_first}Service: ${entity.name}Service) {
    // Retorna o dataSource do resolver
    this.dataSource = this.activatedRoute.snapshot.data['dataSource']
  <#list entity.toOneEntities as toOneEntity>
    this.${toOneEntity.name?uncap_first}DataSource = this.activatedRoute.snapshot.data['${toOneEntity.name?uncap_first}DataSource'];
  </#list>
  <#list entity.toManyEntities as toManyEntity>
    this.${toManyEntity.name?uncap_first}DataSource = this.activatedRoute.snapshot.data['${toManyEntity.name?uncap_first}DataSource'];
  </#list>
    this.dataSource.on({
      'changed': () => this.totalResources = this.dataSource.totalCount()
    });
  }


  create() {
    this.open${entity.name}FormDialog(new ${entity.name}FormData());
  }

  edit(id: number) {
    this.${entity.name?uncap_first}Service.findById(id)
      .pipe(take(1))
      .subscribe((${entity.name?uncap_first}FormData: ${entity.name}FormData) => this.open${entity.name}FormDialog(${entity.name?uncap_first}FormData))
  }

  refresh() {
    this.dataSource.reload()
  }

  private open${entity.name}FormDialog(${entity.name?uncap_first}FormData: ${entity.name}FormData) {
    const dialogRef = this.dialog.open(${entity.name}FormDialogComponent, {
    data: ${entity.name?uncap_first}FormData,
    width: '800px',
    height: 'auto',
  });
<#list entity.toOneEntities as toOneEntity>
  dialogRef.componentInstance.${toOneEntity.name?uncap_first}DataSource = this.${toOneEntity.name?uncap_first}DataSource;
</#list>
<#list entity.toManyEntities as toManyEntity>
  dialogRef.componentInstance.${toManyEntity.name?uncap_first}DataSource = this.${toManyEntity.name?uncap_first}DataSource;
</#list>
  dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result) {
        this.dataSource.reload();
      }
    });
  }

  delete(id) {
    confirm(
      `Você confirma a exclusão do ${entity.name}?`,
      'Confirme a exclusão'
    ).then(result => {
      if (result) {
        this.${entity.name?uncap_first}Service.delete(id)
          .pipe(take(1))
          .subscribe(() => {
            notify(
              "${entity.name} removido com sucesso",
              'success',
              3000
            );
            this.dataGrid.instance.refresh();
          });
      }
    });
  }

  exportCsv() {
    confirm(
      `Deseja exportar os dados de ${entity.name}? <br><b>${r'${this.totalResources}'} registros serão exportado(s).</b>`,
      'Confirme a exportação'
    ).then(result => {
      if (result) {
        this.${entity.name?uncap_first}Service.export(
          {
            sort: this.dataGrid.instance.getDataSource().loadOptions().sort,
            filter: this.dataGrid.instance.getCombinedFilter()
          },
          {
            exportType: DataExportType.CSV,
            columns: this.dataGrid.instance.getVisibleColumns()
              .filter(i => i["dataField"])
              .map(i => i["dataField"])
          }
        )
      }
    });
  }

  /**
   * @see https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onContentReady
   */
  async onContentReady() {
    this.filtered = (
      this.dataSource.isLoaded() &&
      this.dataGrid.instance.getCombinedFilter() &&
      this.dataGrid.instance.getCombinedFilter().length > 0
    ) || false;
  }

  /**
   * Limpa todos os filtros aplicados ao DataGrid
   * @see https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Methods/#clearFilterfilterName
   */
  clearFilters() {
    this.dataGrid.instance.clearFilter('header');
    this.dataGrid.instance.clearFilter('row');
    this.dataGrid.instance.clearFilter('search');
  }

}
