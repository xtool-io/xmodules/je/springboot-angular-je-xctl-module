<#assign repositoryName=entity.name+"Repository">
package ${repositoryPackage};

import ${entity.package}.${entity.name};
import ${rootPackage}.core.persistence.StandardRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ${repositoryName} extends StandardRepository<${entity.name},Long> {
}
