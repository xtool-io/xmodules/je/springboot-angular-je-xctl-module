<#assign tsTypes={"Long": "number", "String": "string", "Integer": "number", "LocalDate": "Date", "LocalDateTime": "Date", "BigDecimal": "number", "Boolean": "boolean"}>
export class ${entity.name}FormData {
<#--<#list entity.attributes as attr>-->
<#--  ${attr.name}?: ${tsTypes[attr.type.name]};-->
<#--</#list>-->
<#--<#list entity.toOneEntities as toOneEntitie>-->
<#--  ${toOneEntitie.name?uncap_first}?: number;-->
<#--</#list>-->
<#--<#list entity.toManyEntities as toManyEntitie>-->
<#--  ${toManyEntitie.name?uncap_first}?: number[];-->
<#--</#list>-->
  <#list entity.fields as field>
    <#if field.hasAnnotation("Column") || field.hasAnnotation("Id")>
      ${field.name}?: ${tsTypes[field.type.name]}
    </#if>
    <#if field.hasAnnotation("ManyToOne") || field.hasAnnotation("OneToOne")>
      ${field.name}?: number;
    </#if>
    <#if field.hasAnnotation("OneToMany") || field.hasAnnotation("ManyToMany")>
      ${field.name}?: number[];
    </#if>
  </#list>
}
