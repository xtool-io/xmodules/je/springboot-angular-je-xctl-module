<#assign tsTypes={"Long": "number", "String": "string", "Integer": "number", "LocalDate": "Date", "LocalDateTime": "Date", "BigDecimal": "number", "Boolean": "boolean"}>
export class ${entity.name}ListView {
<#list listViewEntity.attributes as attr>
    ${attr.name}?:${tsTypes[attr.type.name]}
</#list>
}
