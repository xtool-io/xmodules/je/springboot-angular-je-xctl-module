<#if jpaAttr.type.name == 'String'>
        <!--FIELD ${jpaAttr.name?upper_case} -->
        <dxi-item dataField="${jpaAttr.name}">
            <dxo-label text="${jpaAttr.name}"></dxo-label>
            <div *dxTemplate>
                <dx-text-box
                        [(value)]="formData.${jpaAttr.name}"
                        [hint]="'${jpaAttr.name}'"
                        maxLength="${jpaAttr.maxLength}">

                    <!--VALIDATION-->
                    <dx-validator>
                        <!--MAX LENTGH-->
                        <dxi-validation-rule
                                type="stringLength"
                                [max]="${jpaAttr.maxLength}"
                                message="O campo deve conter no máximo ${jpaAttr.maxLength} caracteres">
                        </dxi-validation-rule>
                        <#if jpaAttr.nullable>
                            <!--REQUIRED-->
                            <dxi-validation-rule
                                    type="required"
                                    message="Este campo é obrigatório">
                            </dxi-validation-rule>
                        </#if>
                        <#if jpaAttr.unique>
                            <!--UNIQUE-->
                            <dxi-validation-rule
                                    type="async"
                                    message="O valor do campo já está registrado na base"
                                    [validationCallback]="${jpaAttr.name}UniqueValidation"
                            ></dxi-validation-rule>
                        </#if>
                    </dx-validator>
                </dx-text-box>
            </div>
        </dxi-item>
</#if>

