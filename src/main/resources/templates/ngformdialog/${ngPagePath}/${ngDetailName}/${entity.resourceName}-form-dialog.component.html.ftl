<#assign basicAttributes=entity.attributes?filter(attr -> attr.hasAnnotation('Column'))>
<#assign manyToOneAttributes=entity.fields?filter(attr -> attr.hasAnnotation('ManyToOne'))>
<#assign OneToManyAttributes=entity.fields?filter(attr -> attr.hasAnnotation('OneToMany'))>
<h1 mat-dialog-title>{{formData.id ? 'Atualização' : 'Criação' }} de ${entity.name}</h1>
<dx-validation-group>
  <div mat-dialog-content>
    <form autocomplete="off">
      <dx-form class="form last plain-styled-form"
               id="form"
               [formData]="formData">
        <#list basicAttributes as jpaAttr>
          <#include '__localdatetime.jpaattr.ftl'>
          <#include '__localdate.jpaattr.ftl'>
          <#include '__string.jpaattr.ftl'>
          <#include '__boolean.jpaattr.ftl'>
          <#include '__integer.jpaattr.ftl'>
        </#list>
        <#list entity.toOneEntities as toOneEntitie>
          <#include '__toone.jpaattr.ftl'>
        </#list>
        <#list entity.toManyEntities as toManyEntitie>
          <#include '__tomany.jpaattr.ftl'>
        </#list>
      </dx-form>
    </form>
  </div>
  <div mat-dialog-actions>
    <dx-button text="Salvar" type="default" (onClick)="save($event)"></dx-button>
    <dx-button text="Cancelar" stylingMode="text" (onClick)="cancel()"></dx-button>
  </div>
</dx-validation-group>

