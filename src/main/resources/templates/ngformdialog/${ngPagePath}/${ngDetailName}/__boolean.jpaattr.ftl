<#if jpaAttr.type.name == 'Boolean'>
        <!--FIELD ${jpaAttr.name?upper_case} -->
        <dxi-item dataField="${jpaAttr.name}" editorType="dxCheckBox">
            <div *dxTemplate>
                <dx-check-box
                    [(value)]="formData.${jpaAttr.name}"
                    [hint]="'${jpaAttr.name}'">
                    <!--VALIDATION-->
                    <dx-validator>
                        <#if jpaAttr.nullable>
                            <!--REQUIRED-->
                            <dxi-validation-rule
                                    type="required"
                                    message="Este campo é obrigatório">
                            </dxi-validation-rule>
                        </#if>
                    </dx-validator>
                </dx-check-box>
            </div>
        </dxi-item>
</#if>

