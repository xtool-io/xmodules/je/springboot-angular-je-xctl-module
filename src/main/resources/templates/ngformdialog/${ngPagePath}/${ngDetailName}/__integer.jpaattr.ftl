<#if jpaAttr.type.name == 'Integer'
    || jpaAttr.type.name == 'Long'
    || jpaAttr.type.name == 'Double'
    || jpaAttr.type.name == 'Float'
    || jpaAttr.type.name == 'BigDecimal'>

        <!--FIELD ${jpaAttr.name?upper_case} -->
        <dxi-item dataField="${jpaAttr.name}">
            <dxo-label text="${jpaAttr.name}"></dxo-label>
            <div *dxTemplate>
                <dx-number-box
                        [(value)]="formData.${jpaAttr.name}"
                        maxLength="${jpaAttr.maxLength}">

                    <!--VALIDATION-->
                    <dx-validator>
                        <dxi-validation-rule
                                type="stringLength"
                                [max]="${jpaAttr.maxLength}"
                                message="O campo deve conter no máximo ${jpaAttr.maxLength} caracteres">
                        </dxi-validation-rule>
                        <#if jpaAttr.nullable>
                            <!--REQUIRED-->
                            <dxi-validation-rule
                                    type="required"
                                    message="Este campo é obrigatório">
                            </dxi-validation-rule>
                        </#if>
                    </dx-validator>
                </dx-number-box>
            </div>
        </dxi-item>
</#if>

