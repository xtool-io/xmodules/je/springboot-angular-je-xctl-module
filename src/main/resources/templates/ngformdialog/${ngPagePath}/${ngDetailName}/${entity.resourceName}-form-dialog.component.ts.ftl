<#assign basicAttributes=entity.attributes?filter(attr -> attr.hasAnnotation('Column'))>
import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {${entity.name}Service} from "../../../../service/${entity.resourceName}.service";
import notify from "devextreme/ui/notify";
import {take} from "rxjs/operators";
import {${entity.name}FormData} from "../../../../domain/${entity.resourceName}-form-data";
import {SharedModule} from "../../../../@shared/shared.module";
import {lastValueFrom} from "rxjs";

@Component({
  selector: 'app-${entity.resourceName}-dialog',
  templateUrl: './${entity.resourceName}-form-dialog.component.html',
  styles: [],
  standalone: true,
  imports: [
    SharedModule
  ]
})
export class ${entity.name}FormDialogComponent {
  formData: ${entity.name}FormData;
<#list entity.toOneEntities as toOneEntity>
  ${toOneEntity.name?uncap_first}DataSource: any;
</#list>
<#list entity.toManyEntities as toManyEntity>
  ${toManyEntity.name?uncap_first}DataSource: any;
</#list>

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: ${entity.name}FormData,
    public dialogRef: MatDialogRef<${entity.name}FormDialogComponent>,
    protected ${entity.name?uncap_first}Service: ${entity.name}Service
  ) {
    this.dialogRef.disableClose = true
    this.formData = data;
  }

  cancel() {
    this.dialogRef.close(false);
  }

  <#list basicAttributes as jpaAttr>
  <#if jpaAttr.unique>
  ${jpaAttr.name}UniqueValidation = (params): Promise<void> => {
    return new Promise<void>((resolve, reject) => {
      lastValueFrom(this.${entity.name?uncap_first}Service.findByExample({${jpaAttr.name}: params.value}))
      .then( (value)  => value.id ? reject() : resolve())
      .catch(error => {
      reject();
      throw new Error(error.message);
      });
      });
      };
      </#if>
      </#list>


      /**
   * Metodo para salvar o ${entity.name}
   */
  save(e) {
    const message = this.formData['id'] ? "As alterações de ${entity.name} foram realizadas." : "${entity.name} foi criado com sucesso!";
    this.${entity.name?uncap_first}Service.save(this.formData).pipe(take(1))
      .subscribe((resource: ${entity.name}FormData) => {
        notify(
          {
            message,
            position: 'top center',
          },
          'success',
          5000,
        );
        this.dialogRef.close(resource);
      }, error => {
            notify(
                {
                  message: 'Por favor, revise o formulário, pois foram identificados erros. Corrija-os antes de prosseguir;',
                  position: 'bottom center',
                },
                'error',
                5000,
            );
          }
      );
  }
}
