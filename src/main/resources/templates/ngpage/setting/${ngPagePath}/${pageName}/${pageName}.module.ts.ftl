import { NgModule } from '@angular/core';
import { ${entity.name}PageRoutingModule } from './${entity.resourceName}-page-routing.module';
import { ${entity.name}PageComponent } from './${entity.resourceName}-page.component';
import { SharedModule } from '../../../@shared/shared.module';
import { ${entity.name}DataGridComponent } from "./${entity.resourceName}-list/${entity.resourceName}-data-grid.component";
import { NgxPermissionsModule } from 'ngx-permissions';

@NgModule({
  declarations: [
    ${entity.name}PageComponent,
    ${entity.name}DataGridComponent,
  ],
  imports: [
    ${entity.name}PageRoutingModule,
    SharedModule,
    NgxPermissionsModule.forChild(), 
  ],
})
export class ${entity.name}PageModule {}
