import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ${entity.name}PageComponent } from './${pageName}.component';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { ${entity.name}DataGridComponent } from "./${entity.resourceName}-list/${entity.resourceName}-data-grid.component";
import { ${entity.name}DataSourceResolver
} from "../../../resolver/${entity.resourceName}-data-source.resolver";
<#list entity.toOneEntities as toOneEntitie>
import { ${toOneEntitie.name}DataSourceResolver } from "app/resolver/${toOneEntitie.resourceName}-data-source.resolver";
</#list>
<#list entity.toManyEntities as toManyEntitie>
import { ${toManyEntitie.name}DataSourceResolver } from "app/resolver/${toManyEntitie.resourceName}-data-source.resolver";
</#list>

const routes: Routes = [
  {
    path: '',
    component: ${entity.name}PageComponent,
    children: [
      { path: '',
        component: ${entity.name}DataGridComponent,
        resolve: {
          dataSource: ${entity.name}DataSourceResolver,
        <#list entity.toOneEntities as toOneEntitie>
          ${toOneEntitie.name?uncap_first}DataSource: ${toOneEntitie.name}DataSourceResolver,
        </#list>
        <#list entity.toManyEntities as toManyEntitie>
          ${toManyEntitie.name?uncap_first}DataSource: ${toManyEntitie.name}DataSourceResolver,
        </#list>
        }
      },
<#--      {-->
<#--        path: 'new',-->
<#--        children: [-->
<#--          {-->
<#--            path: '',-->
<#--            component: ${entity.name}PageDetailComponent,-->
<#--            resolve: { formData: ${entity.name}PageResolver },-->
<#--            canActivate: [NgxPermissionsGuard],-->
<#--            data: {-->
<#--              permissions: {-->
<#--                only: '${entity.resourceName}:resource:create',-->
<#--              },-->
<#--            },-->
<#--          },-->
<#--        ],-->
<#--      },-->
<#--      {-->
<#--        path: 'detail',-->
<#--        children: [-->
<#--          {-->
<#--            path: ':id',-->
<#--            component: ${entity.name}PageDetailComponent,-->
<#--            resolve: { formData: ${entity.name}PageResolver },-->
<#--            canActivate: [NgxPermissionsGuard],-->
<#--            data: {-->
<#--              permissions: {-->
<#--                only: '${pageName}:resource:view',-->
<#--              },-->
<#--            },-->
<#--          },-->
<#--        ],-->
<#--      },-->
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ${entity.name}PageRoutingModule {}
