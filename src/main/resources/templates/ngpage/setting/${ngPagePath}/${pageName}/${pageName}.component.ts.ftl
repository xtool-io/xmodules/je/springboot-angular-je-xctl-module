import { Component } from '@angular/core';

@Component({
  selector: 'app-${pageName}-page',
  template: `
    <div style="height: 100%">
      <router-outlet></router-outlet>
    </div>
  `,
})
export class ${pageClassName}Component {}
