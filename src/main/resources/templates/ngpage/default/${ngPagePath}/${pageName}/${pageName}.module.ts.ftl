import { NgModule } from '@angular/core';
import { ${entity.name}PageRoutingModule } from './${entity.resourceName}-page-routing.module';
import { ${entity.name}PageComponent } from './${entity.resourceName}-page.component';
import { SharedModule } from '../../@shared/shared.module';
import { ${entity.name}DataGridComponent } from "./${entity.resourceName}-list/${entity.resourceName}-data-grid.component";
import { NgxPermissionsModule } from 'ngx-permissions';
import {${entity.name}FormCardComponent} from "./${entity.resourceName}-detail/${entity.resourceName}-form-card.component";


@NgModule({
  declarations: [
    ${entity.name}PageComponent,
    ${entity.name}DataGridComponent,
    ${entity.name}FormCardComponent,
  ],
  imports: [
    ${entity.name}PageRoutingModule,
    SharedModule,
    NgxPermissionsModule.forChild(), 
  ],
})
export class ${entity.name}PageModule {}
