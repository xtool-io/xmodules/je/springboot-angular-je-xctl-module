package ${rootPackage}.app.rest;

import ${rootPackage}.app.domain.${entity.name};
import ${rootPackage}.app.repository.${entity.name}Repository;
import ${rootPackage}.app.repository.vw.${entity.name}ListViewRepository;
import br.jus.tre_pa.core.persistence.dataexport.DataExportOptions;
import br.jus.tre_pa.core.persistence.dataexport.DataExportResult;
import br.jus.tre_pa.core.persistence.datafilter.RSQLParam;
import br.jus.tre_pa.core.persistence.datafilter.model.PageEx;
import br.jus.tre_pa.core.persistence.datafilter.model.SummaryOptions;
import br.jus.tre_pa.core.persistence.datafilter.model.SummaryResult;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.List;

/**
 * Classe REST com os endpoints para a entidade {@link ${entity.name}}
 */
@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/api/${entity.resourceName}")
public class ${entity.name}Rest {

    private final ${entity.name}ListViewRepository ${entity.name?uncap_first}ListViewRepository;

    private final ${entity.name}Repository ${entity.name?uncap_first}Repository;

    private final ModelMapper mapper;

    /**
     * Endpoint que retorna uma lista paginada com suporte a ordenação, filtragem de dados e sumarização.
     * <h1>1. Paginação</h1>
     *
     * <p>Exemplo de requisição para retornar a primeira página (parâmetro <i>page</i>)
     * com 20 registos (parâmetro <i>size</i>):</p>
     * <pre>{@code
     * $ ./kurl.sh "http://localhost:8080/appng15/api/${entity.resourceName}?page=0&size=20"
     * }</pre>
     *
     *
     * <h1>2. Ordenação</h1>
     *
     * <h1>3. Filtragem de dados</h1>
     *
     * <h1>4. Sumarização</h1>
     *
     * <h1>5. Segurança </h1>
     *
     * @param q              Objeto {@link RSQLParam} com os parâmetros de filtragem.
     * @param pageable       Objeto {@link Pageable} com o parâmetros de paginação e ordenação.
     * @param summaryOptions Objeto {@link SummaryOptions} com os parâmetros de sumarização.
     * @return Objeto {@link PageEx} para entidade {@link ${rootPackage}.app.domain.vw.${entity.name}ListView}
     */
    @GetMapping
    public ResponseEntity<PageEx<?>> findAll(RSQLParam q, Pageable pageable, SummaryOptions summaryOptions) {
        return ResponseEntity.ok(
            ${entity.name?uncap_first}ListViewRepository.findAll(
                q.getSpecification(),
                pageable,
                summaryOptions
            )
        );
    }

    /**
     * Endpoint que retorna operações de sumarização (sum, max, min, count, dcount e avg) para o(s) registro(s) da entidade {@link ${rootPackage}.app.domain.vw.${entity.name}ListView}
     *
     * @param q
     * @param summaryOptions
     * @return
     */
    @GetMapping("/summarize")
    public ResponseEntity<List<SummaryResult>> summarize(RSQLParam q, SummaryOptions summaryOptions) {
        return ResponseEntity.ok(
            ${entity.name?uncap_first}ListViewRepository.summarize(q.getSpecification(), summaryOptions)
        );
    }

    /**
     * Endpoint retorna um único registro da entidade {@link ${entity.name}}.
     * <p>A busca pelo registro é feita pelo Id da entidade e caso o Id especificado não exista na base de dados a exception {@link EntityNotFoundException} é lançada.</p>
     *
     * @param id Id da entidade {@link ${entity.name}}
     * @return Entidade {@link ${entity.name}}
     */
    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id) {
        return ResponseEntity.ok(
            ${entity.name?uncap_first}Repository.findById(id).orElseThrow(EntityNotFoundException::new)
        );
    }

    /**
     * Endpoint retorna um único registro da entidade {@link ${entity.name}}.
     *
     * @param resource
     * @return
     */
    @PostMapping("/by-example")
    public ResponseEntity<?> findByExample(@RequestBody ${entity.name} resource) {
        Example<${entity.name}> example = Example.of(resource);
        return ResponseEntity.ok(
            ${entity.name?uncap_first}Repository.findOne(example).orElseGet(${entity.name}::new)
        );
    }

    /**
     * Endpoint que realiza a exportação em arquivo .csv dos registros da entidade {@link ${rootPackage}.app.domain.vw.${entity.name}ListView}.
     *
     * @param q
     * @param sort
     * @param dataExportOptions
     * @return
     */
    @PostMapping("/export")
    public ResponseEntity<Resource> export(RSQLParam q, Sort sort, @RequestBody DataExportOptions dataExportOptions) {
        DataExportResult dataExportResult = ${entity.name?uncap_first}ListViewRepository.export(q.getSpecification(), sort, dataExportOptions);
        return dataExportResult.toResponseEntity();
    }

    <#if !entity.readOnly>
    /**
     * Endpoint que realiza a criação de um novo {@link ${entity.name}}
     *
     * @param resource Objeto {@link ${entity.name}} com os atributos a serem criados na base de dados.
     * @return
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> insert(@Valid @RequestBody ${entity.name} resource) {
        return ResponseEntity.ok(${entity.name?uncap_first}Repository.save(resource));
    }

    /**
     * Endpoint que realiza a atualização de um recurso já existente na base de dados.
     *
     * @param id
     * @param resource
     * @return
     */
    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @Valid @RequestBody ${entity.name} resource) {
        log.info("Requisição de atualização do registro ${entity.name}(id={})", id);
        if (!${entity.name?uncap_first}Repository.existsById(id)) throw new EntityNotFoundException();
        return ResponseEntity.ok(${entity.name?uncap_first}Repository.save(resource));
    }

    /**
     * Endpoint que realiza a exclusão do recurso na base de dados.
     *
     * @param id Id do recurso a ser excluído.
     */
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        log.info("Requisição de exclusão do registro ${entity.name}(id={})", id);
        if (!${entity.name?uncap_first}Repository.existsById(id)) throw new EntityNotFoundException();
        ${entity.name?uncap_first}Repository.deleteById(id);
    }

    /**
     * Endpoint para a realização de atualização(s) parcial(s) nos atributos da entidade {@link ${entity.name}}
     *
     * @param id       Id da entidade {@link ${entity.name}} a ser atualizada.
     * @param resource Objeto {@link ${entity.name}} com os atributos a serem atualizados.
     * @return Entidade {@link ${entity.name}} com todos os atributos
     */
    @PatchMapping(path = "/{id}")
    public ResponseEntity<?> patch(@PathVariable Long id, @RequestBody ${entity.name} resource) {
        log.info("Requisição de atualização do registro ${entity.name}(id={})", id);
        ${entity.name} ${entity.name?uncap_first} = ${entity.name?uncap_first}Repository.findById(id).orElseThrow(EntityNotFoundException::new);
        mapper.map(resource, ${entity.name?uncap_first});
        return ResponseEntity.ok(
            ${entity.name?uncap_first}Repository.save(${entity.name?uncap_first})
        );
    }
    </#if>

}
