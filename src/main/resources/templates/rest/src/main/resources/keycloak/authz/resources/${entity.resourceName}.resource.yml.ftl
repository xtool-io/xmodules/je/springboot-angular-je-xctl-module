---
resource:
  clientId: ${projectName}-backend
  name: "${entity.resourceName}:resource"
  uris: ["/api/${entity.resourceName}/*"]
  scopes: ["view", "create", "update", "delete"]
