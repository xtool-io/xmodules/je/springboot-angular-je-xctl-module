---
scopePermission:
    clientId: ${projectName}-backend
    name: "${entity.resourceName}:permission:view"
    description: Permissão de visualização do recurso ${entity.name}.
    resources: ["${entity.resourceName}:resource"]
    scopes: ["view"]
    policies: ["auth:js:policy"]
---
scopePermission:
    clientId: ${projectName}-backend
    name: "${entity.resourceName}:permission:modify"
    description: Permissão de modificação (criar, atualizar ou deletar) do recurso ${entity.name}.
    resources: ["${entity.resourceName}:resource"]
    scopes: ["create", "update", "delete"]
    policies: ["auth:js:policy"]
