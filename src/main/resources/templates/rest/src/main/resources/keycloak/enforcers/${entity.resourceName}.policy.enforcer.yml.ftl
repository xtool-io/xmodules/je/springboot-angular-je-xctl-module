policy-enforcer-config:
  paths:
  - path: /api/${entity.resourceName}/*
    methods:
      - method: GET
        scopes: [ "view" ]
      - method: POST
        scopes: [ "create" ]
  - path: /api/${entity.resourceName}/{id}
    methods:
      - method: GET
        scopes: [ "view" ]
      - method: PUT
        scopes: [ "update" ]
      - method: DELETE
        scopes: [ "delete" ]
