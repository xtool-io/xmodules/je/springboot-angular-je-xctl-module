<#assign tableColumn=plantClass.attributes?map(attr -> attr.name?upper_case?truncate(30))>
package ${rootPackage}.app.domain.vw;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import javax.persistence.*;
import java.time.*;
import java.util.*;
import java.math.BigDecimal;
import javax.persistence.Entity;
import org.hibernate.annotations.Subselect;

@Entity
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@ToString
@Subselect("""
   SELECT
      ${tableColumn?join(",\n\t\t")}
   FROM ${vwName}
""")
public class ${plantClass.name}ListView {

   @Id
   private Long id;

<#list plantClass.attributes as attr>
   <#if plantTask.isId(attr)><#continue></#if>
   @Column(name="${attr.name?upper_case?truncate(30)}")
   private ${attr.type} ${attr.name};
</#list>

}
