<#assign isManyToMany = ["0..*", "1..*"]?seq_contains(rel.multiplicities.first) && ["0..*", "1..*"]?seq_contains(rel.multiplicities.second)>
<#assign isRequired = rel.multiplicities.second == "1..*">
<#assign sourceTableName = rel.firstEntity.name?upper_case?truncate(14)>
<#assign targetTableName = rel.secondEntity.name?upper_case?truncate(15)>
<#assign targetEntityName = rel.secondEntity.name>
<#if isManyToMany>
   /**
    *
    */
   @ManyToMany
   @BatchSize(size=10)
   <#if isRequired>@Size(min=1)</#if>
   @JoinTable(name="${sourceTableName}_${targetTableName}",
      joinColumns = @JoinColumn(name = "${sourceTableName}_ID"),
      inverseJoinColumns = @JoinColumn(name = "${targetTableName}_ID"))
   @JsonDeserialize(contentUsing=${targetEntityName}Databind.IdDeserializer.class)
   @JsonSerialize(contentUsing=${targetEntityName}Databind.IdSerializer.class)
   private Set<${rel.secondEntity.name}> ${rel.secondEntity.name?uncap_first} = new HashSet<>();
</#if>
