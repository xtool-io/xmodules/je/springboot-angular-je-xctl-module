<#assign columnName = attr.name?upper_case?truncate(30)>
<#if attr.type == 'Float'>
   /**
    *
    */
   @Column(name="${columnName}")
   private ${attr.type} ${attr.name};
</#if>
