package ${rootPackage}.app.domain.databind;

import ${rootPackage}.app.domain.${plantClass.name};
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.*;

import java.io.IOException;

public class ${plantClass.name}Databind {
    public static class IdDeserializer extends JsonDeserializer<${plantClass.name}> {
        @Override
        public ${plantClass.name} deserialize(JsonParser jp, DeserializationContext deserializationContext) throws IOException {
            JsonNode node = jp.getCodec().readTree(jp);
            if (node.isNumber()) {
                ${plantClass.name} c = new ${plantClass.name}();
                c.setId(node.asLong());
                return c;
            } else if (node.isObject()) {
                JsonNode id = node.get("id");
                ${plantClass.name} c = new ${plantClass.name}();
                c.setId(id.asLong());
                return c;
            }
            return null;
        }
    }

    public static class IdSerializer extends JsonSerializer<${plantClass.name}> {
        @Override
        public void serialize(${plantClass.name} entity, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws
                IOException {
            jsonGenerator.writeNumber(entity.getId());
        }
    }
}


