<#assign isManyToOne = ["0..*", "1..*"]?seq_contains(rel.multiplicities.first) && ["0", "0..1", "1"]?seq_contains(rel.multiplicities.second)>
<#assign isRequired = rel.multiplicities.second == "1">
<#assign joinColumnName = rel.secondEntity.name?upper_case?truncate(27)+"_ID">
<#assign targetEntityName = rel.secondEntity.name>
<#if isManyToOne>
   /**
    *
    */
   @ManyToOne(fetch = FetchType.LAZY ${isRequired?string(",optional=false","")})
   @JoinColumn(name="${joinColumnName}" ${isRequired?string(", nullable=false","")})
   @JsonDeserialize(using=${targetEntityName}Databind.IdDeserializer.class)
   @JsonSerialize(using=${targetEntityName}Databind.IdSerializer.class)
   private ${rel.secondEntity.name} ${rel.secondEntity.name?uncap_first};
</#if>
