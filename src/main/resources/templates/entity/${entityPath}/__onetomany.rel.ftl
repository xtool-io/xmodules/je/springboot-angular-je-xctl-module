<#assign isOneToMany = ["0", "0..1", "1"]?seq_contains(rel.multiplicities.first) && ["0..*", "1..*"]?seq_contains(rel.multiplicities.second)>
<#assign isComposition = rel.composition>
<#assign isRequired = rel.multiplicities.second == "1..*">
<#assign joinColumnName = rel.firstEntity.name?upper_case?truncate(27)+"_ID">
<#assign targetEntityName = rel.secondEntity.name>
<#assign isSelfAssociation = rel.selfAssociation>
<#if isOneToMany && !isSelfAssociation>
    /**
     *
     */
    @OneToMany${isComposition?string("(cascade = CascadeType.ALL, orphanRemoval = true)", "")}
    @BatchSize(size=10)
    <#if isRequired>@Size(min=1)</#if>
    @JoinColumn(name="${joinColumnName}" ${isRequired?string(", nullable=false","")})
    @JsonDeserialize(contentUsing=${targetEntityName}Databind.IdDeserializer.class)
    @JsonSerialize(contentUsing=${targetEntityName}Databind.IdSerializer.class)
    private List<${rel.secondEntity.name}> ${rel.secondEntity.name?uncap_first} = new ArrayList<>();
</#if>
<#if isOneToMany && isSelfAssociation>
    /**
     *
     */
    @OneToMany${isSelfAssociation?then("(mappedBy=\"${rel.secondEntity.name?uncap_first}Parent\")", "")}
    @BatchSize(size=10)
    <#if isRequired>@Size(min=1)</#if>
    @JsonDeserialize(contentUsing=${targetEntityName}Databind.IdDeserializer.class)
    @JsonSerialize(contentUsing=${targetEntityName}Databind.IdSerializer.class)
    private List<${rel.secondEntity.name}> ${rel.secondEntity.name?uncap_first} = new ArrayList<>();

    /**
     *
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="${rel.secondEntity.name?upper_case?truncate(21)}PARENT_ID")
    @JsonDeserialize(using=${targetEntityName}Databind.IdDeserializer.class)
    @JsonSerialize(using=${targetEntityName}Databind.IdSerializer.class)
    private ${rel.secondEntity.name} ${rel.secondEntity.name?uncap_first}Parent;
</#if>
