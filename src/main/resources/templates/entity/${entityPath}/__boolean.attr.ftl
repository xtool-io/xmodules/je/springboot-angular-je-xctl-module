<#--Fragmento de template com a definição de atributos do tipo Boolean -->
<#--attr: Instância de PlantField -->
<#assign columnName = attr.name?upper_case?truncate(30)>
<#if attr.type == 'Boolean'>
   /**
    *
    */
   @Column(name="${columnName}", columnDefinition = "NUMBER(1,0) DEFAULT 0")
   private ${attr.type} ${attr.name};
</#if>
