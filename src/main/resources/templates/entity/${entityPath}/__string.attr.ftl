<#assign isUnique = attr.properties?seq_contains("unique")>
<#assign isNotNull = attr.properties?seq_contains("notnull")>
<#assign hasMultiplicity = attr.multiplicity??>
<#assign columnName = attr.name?upper_case?truncate(30)>
<#assign colNullableAttr = isNotNull?then(", nullable=false", "")>
<#assign colUniqueAttr = isUnique?then(", unique=true", "")>
<#assign colLengthAttr = hasMultiplicity?then(", length=${attr.multiplicity}", "")>
<#if attr.type == 'String'>
   /**
    *
    */
   <#if isNotNull>@NotNull</#if>
   <#if hasMultiplicity>@Size(max=${attr.multiplicity})</#if>
   @Column(name="${columnName}"${colNullableAttr}${colUniqueAttr}${colLengthAttr})
   private ${attr.type} ${attr.name};
</#if>
