<#assign columnName = attr.name?upper_case?truncate(30)>
<#if attr.type == 'Double'>
   /**
    *
    */
   @Column(name="${columnName}")
   private ${attr.type} ${attr.name};
</#if>
