<#--<#assign tableName=plantClass.name?upper_case?truncate(30)>-->
package ${rootPackage}.app.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.*;
import org.hibernate.annotations.*;
import java.util.*;
import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.Table;
import com.fasterxml.jackson.databind.annotation.*;
import ${rootPackage}.app.domain.databind.*;
import javax.persistence.CascadeType;
${plantClass.stereotypes?seq_contains('Setting')?then('import ${rootPackage}.core.annotation.SettingEntity;', '')}

@Entity
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@Table(name = "${tableName}")
@DynamicUpdate
@DynamicInsert
@ToString(of = "id")
${plantClass.stereotypes?seq_contains('Setting')?string('@SettingEntity', '')}
public class ${plantClass.name} {

   @Id
   @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_${tableName?truncate(26)}")
   @SequenceGenerator(initialValue = 1, allocationSize = 1, name = "SEQ_${tableName?truncate(26)}")
   private Long id;

<#list plantClass.attributes as attr>
   <#include '__string.attr.ftl'>
   <#include '__boolean.attr.ftl'>
   <#include '__long.attr.ftl'>
   <#include '__integer.attr.ftl'>
   <#include '__localdate.attr.ftl'>
   <#include '__localdatetime.attr.ftl'>
   <#include '__bigdecimal.attr.ftl'>
</#list>

<#list plantRelationships as rel>
   <#include '__onetomany.rel.ftl'>
   <#include '__manytomany.rel.ftl'>
   <#include '__manytoone.rel.ftl'>
</#list>

}
