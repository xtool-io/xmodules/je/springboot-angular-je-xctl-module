<#--<#assign tableName=plantClass.name?upper_case?truncate(30)>-->
<#assign tableColumn=plantClass.attributes?map(attr -> attr.name?upper_case?truncate(30))>
CREATE OR REPLACE VIEW ${vwName} AS
SELECT
${tableColumn?map(attr -> "T."+attr)?join(", ")}
FROM ${tableName} T
