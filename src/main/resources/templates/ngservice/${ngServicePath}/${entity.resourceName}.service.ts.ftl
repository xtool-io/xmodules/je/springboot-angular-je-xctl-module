import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from "@angular/common/http";
import {LoadOptions} from "devextreme/data";
import { environment } from 'environments/environment';
import {catchError, map, take} from "rxjs/operators";
import {HttpParamsAdapter} from "../@core/types/http-params-adapter";
import {saveAs} from 'file-saver';
import {PageEx} from "../@core/types/page-ex";
import {DataExportOptions} from "../@core/dataexport/data-export-options";
import {Observable, throwError} from "rxjs";
import {${entity.name}FormData} from "../domain/${entity.resourceName}-form-data";

@Injectable({
    providedIn: 'root'
})
export class ${entity.name}Service {

    URL_API: string = `${r'${environment.contextPath}/api/'}${entity.resourceName}`;

    protected constructor(protected http: HttpClient) {
    }

    public findAll(loadOptions: LoadOptions): Observable<{ data: ${entity.name}FormData[]; totalCount: number }> {
        const params = new HttpParamsAdapter(loadOptions).httpParams();
        return this.http.get<PageEx<${entity.name}FormData>>(this.URL_API, {params})
            .pipe(
                take(1),
                map((page: PageEx<${entity.name}FormData>) => ({
                    data: page.content,
                    totalCount: page.totalElements,
                    summary: page.summaries?.map(s => s.result)
                })),
                catchError(error => {
                    return throwError(() => new Error(error.message))
                })
            )
    }

    public findById(id: number): Observable<any> {
        return this.http.get<${entity.name}FormData>(`${r'${this.URL_API}/${id}'}`);
    }

    public save(resource: ${entity.name}FormData): Observable<${entity.name}FormData> {
        return resource['id'] ? this.update(resource['id'], resource) : this.insert(resource);
    }

    public insert(resource: ${entity.name}FormData): Observable<${entity.name}FormData> {
        return this.http.post<${entity.name}FormData>(this.URL_API, resource);
    }

    public update(id: number, resource: ${entity.name}FormData): Observable<${entity.name}FormData> {
        return this.http.put<${entity.name}FormData>(`${r'${this.URL_API}/${id}'}`, resource);
    }

    public patch(id: number, resource: Partial<${entity.name}FormData>) {
        return this.http.patch(`${r'${this.URL_API}/${id}'}`, resource);
    }

    public delete(id: number): Observable<void> {
        return this.http.delete<void>(`${r'${this.URL_API}/${id}'}`);
    }

    public findByExample(resource: ${entity.name}FormData){
        return this.http.post<${entity.name}FormData>(`${r'${this.URL_API}/by-example'}`, resource);
    }



    public export(loadOptions: LoadOptions, dataExportOptions: DataExportOptions) {
        console.log("LoadOption: ", loadOptions)
        const params = new HttpParamsAdapter(loadOptions).httpParams();
        return this.http.post(`${r'${this.URL_API}'}/export`, dataExportOptions,
            {
                responseType: 'blob',
                observe: 'response',
                params: params
            })
            .pipe(take(1))
            .subscribe(httpResponse =>
                saveAs(
                    httpResponse.body,
                    this.getFilenameFromHttpResponse(httpResponse))
            )
    }

    private getFilenameFromHttpResponse(httpResponse: HttpResponse<any>): string {
        const contentDisposition = httpResponse.headers.get('content-disposition');
        return contentDisposition.split(';')[1].split('filename')[1].split('=')[1].trim()
            .replace("\"", "")
            .replace("\"", "");
    }

}
