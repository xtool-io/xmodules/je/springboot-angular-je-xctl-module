version: "2.4"
services:
  ${projectName}-oracle12:
    image: git.tre-pa.jus.br:4567/sds/infra/oracle12-ee-tre-pa:d9362acc
    container_name: ${projectName}-oracle12
    volumes:
      - ./src/main/resources/docker/oracle:/opt/oracle/scripts/startup
    environment:
      - ORACLE_DISABLE_ASYNCH_IO=true
      - ORACLE_ALLOW_REMOTE=true
    ports:
      - 1521:1521
    networks:
      - ${projectName}-net
  ${projectName}-keycloak:
    image: jboss/keycloak:13.0.1
    container_name: ${projectName}-keycloak13.0.1
    command: [ "-b", "0.0.0.0",
                "-Dkeycloak.migration.action=import",
                "-Dkeycloak.migration.provider=dir",
                "-Dkeycloak.migration.dir=/tmp/jboss/keycloak/realm-config",
                "-Dkeycloak.migration.strategy=IGNORE_EXISTING",
                "-Djboss.socket.binding.port-offset=5",
                "-Dkeycloak.profile.feature.upload_scripts=enabled" ]
    volumes:
      - ./src/main/resources/docker/keycloak/realm-config:/tmp/jboss/keycloak/realm-config
      - ~/apps01/kc11.0.1/db:/tmp/jboss/keycloak/standalone/data
    environment:
      - KEYCLOAK_USER=admin
      - KEYCLOAK_PASSWORD=admin
      - DB_VENDOR=h2
      - KEYCLOAK_IMPORT=/tmp/jboss/keycloak/realm-config/TRE-PA-realm.json
    ports:
      - 8085:8085
      - 8448:8448
      - 9995:9995
  ${projectName}-sonarqube:
    image: sonarqube:10.2.1-community
    container_name: ${projectName}-sonarqube
    ports:
      - 9000:9000
  ${projectName}-prometheus:
    image: prom/prometheus:v2.35.0
    network_mode: host
    container_name: ${projectName}-prometheus
    restart: unless-stopped
    volumes:
        - ./prometheus.yml:/etc/prometheus/prometheus.yml
    command:
        - '--config.file=/etc/prometheus/prometheus.yml'
  ${projectName}-grafana:
    image: grafana/grafana-oss:10.1.1
    pull_policy: always
    network_mode: host
    user: "$UID:$GID"
    container_name: ${projectName}-grafana
    restart: unless-stopped
    volumes:
        - ./src/main/resources/docker/grafana:/etc/grafana
        - ~/apps01/grafana10.1.1:/var/lib/grafana
    environment:
        - GF_SECURITY_ADMIN_USER=admin
        - GF_SECURITY_ADMIN_PASSWORD=admin
        - GF_SERVER_DOMAIN=localhost
networks:
  ${projectName}-net:
    name: ${projectName}-net
