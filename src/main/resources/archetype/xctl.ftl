#!/bin/bash

GROUP_ID="je-xctl-module"
ARTIFACT_ID="${buildProperties.name}"
VERSION="${buildProperties.version}"
MVN_REPO="https://gitlab.com/api/v4/projects/47326492/packages/maven"
NVM_VERSION="v0.39.3"
JAVA_VERSION="17.0.8"
SDKMAN_JAVA_VERSION="8.0.302-open"
NODE_VERSION="18.17.0"

if ! command -v java >/dev/null; then
echo "Ferramenta java não encontrada."
echo "======================================================================================================"
echo " Por favor, instale o java com o utilitário sdk (https://sdkman.io/): "
echo ""
echo " $ sdk install $SDKMAN_JAVA_VERSION"
echo ""
echo " Após a instalação repita a operação."
echo "======================================================================================================"
echo ""
exit 1
fi

if [[ "$(java -version 2>&1 | awk -F '"' '/version/ {print $2}')" != "$JAVA_VERSION"* ]]; then
echo "======================================================================================================"
echo "Para o correto funcionamento do projeto é necessário a versão $JAVA_VERSION da ferramenta java."
echo ""
echo "$ sdk use java $JAVA_VERSION"-oracle
echo ""
echo " Após a instalação repita a operação."
echo "======================================================================================================"
exit 1
fi

if ! command -v mvn >/dev/null; then
echo "Ferramenta maven não encontrada."
echo "======================================================================================================"
echo " Por favor, instale a ferramenta maven com o utilitário sdk (https://sdkman.io/): "
echo ""
echo " $ sdk install maven"
echo ""
echo " Após a instalação repita a operação."
echo "======================================================================================================"
echo ""
exit 1
fi

if ! command -v yq >/dev/null; then
echo "Ferramenta yq não encontrada."
echo "======================================================================================================"
echo " Por favor, instale a ferramenta yq com os procedimentos encontrado no endereço abaixo: "
echo " MacOS: brew install yq"
echo " Linux: wget https://github.com/mikefarah/yq/releases/download/v4.31.1/yq_linux_amd64.tar.gz -O - | tar xz && sudo mv yq_linux_amd64 /usr/bin/yq"
echo ""
echo " Após a instalação repita a operação."
echo "======================================================================================================"
echo ""
exit 1
fi

if [[ ! -d "$HOME"/.nvm ]]; then
echo "Ferramenta nvm não encontrada."
echo "======================================================================================================"
echo " Por favor, instale a ferramenta nvm com o utilitário nvm (https://github.com/nvm-sh/nvm#installing-and-updating): "
echo ""
echo " $ curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/$NVM_VERSION/install.sh | bash"
echo ""
echo " Após a instalação repita a operação."
echo "======================================================================================================"
echo ""
exit 1
fi

if ! command -v node >/dev/null; then
echo "Ferramenta node não encontrada."
echo "======================================================================================================"
echo " Por favor, instale a ferramenta node com o utilitário nvm: "
echo ""
echo " $ nvm install $NODE_VERSION"
echo ""
echo " Após a instalação repita a operação."
echo "======================================================================================================"
echo ""
exit 1
fi

if [[ "$(node -v 2>/dev/null)" != "v$NODE_VERSION" ]]; then
echo "======================================================================================================"
echo "Para o correto funcionamento do projeto é necessário a versão $NODE_VERSION da ferramenta node."
echo ""
echo "$ nvm use $NODE_VERSION"
echo ""
echo " Após a instalação repita a operação."
echo "======================================================================================================"
exit 1
fi

if [[ ! -f "$HOME/.m2/repository/$GROUP_ID/$ARTIFACT_ID/$VERSION/$ARTIFACT_ID-$VERSION.jar" ]]; then
  mvn dependency:get -Dartifact="$GROUP_ID:$ARTIFACT_ID:$VERSION" -DremoteRepositories="$MVN_REPO" -q
fi

java -jar "$HOME/.m2/repository/$GROUP_ID/$ARTIFACT_ID/$VERSION/$ARTIFACT_ID-$VERSION.jar" "$@"
