{
    "core": [
        "file://../../.cldr/cldr-core-30.0.3.zip",
        "file://../../.cldr/cldr-dates-modern-30.0.3.zip",
        "file://../../.cldr/cldr-localenames-modern-30.0.3.zip",
        "file://../../.cldr/cldr-misc-modern-30.0.3.zip",
        "file://../../.cldr/cldr-numbers-modern-30.0.3.zip",
        "file://../../.cldr/cldr-segments-modern-30.0.3.zip",
        "file://../../.cldr/cldr-units-modern-30.0.3.zip"
    ]
}
