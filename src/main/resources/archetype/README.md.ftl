<#assign schemaName=projectName?replace('_', '')?replace('-', '')?truncate(30, '') >
# ${projectName}

## Procedimentos para execução local da aplicação

**Passo 1.** Iniciando o docker compose com os serviços do Oracle e Keycloak:

```shell
$ sudo docker compose up
```

> Em algumas distribuições linux o comando poderá ser `$ sudo docker-compose up`

Após a execução com sucesso o Oracle ficará disponível no endereço: 1521 e ${schemaName}/${schemaName} como usuário e senha.

O Keycloak estará disponível em `localhost:8085/auth` e admin/admin como usuário e senha.

**Passo 2.** Iniciando o frontend

```shell
$ nvm use
$ npx ng s
```

Após a execução com sucesso o frontend poderá ser acessado no endereço `localhost:4200` e root/root como usuário e senha.

**Passo 3.** Iniciando o banckend

```shell
$ sdk env
$ mvn clean spring-boot:run -s .m2/settings.xml
```

Após a execução com sucesso o backend ficará disponível no endereço `localhost:8080`.
