package ${env.getProperty('root.package')}.core.audit;

import lombok.AllArgsConstructor;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorProvider")
@AllArgsConstructor
public class JpaAuditConfig {

    private final KeycloakSecurityContext keycloakSecurityContext;

    @Bean
    AuditorAware<String> auditorProvider() {
        return new AppAuditorAware(keycloakSecurityContext);
    }
}
