package ${env.getProperty('root.package')}.core.log;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Configuração para log de requisições REST.
 */
@Configuration
@AllArgsConstructor
public class RestLoggingConfig implements WebMvcConfigurer {

    private final RestLoggingInterceptor restLoggingInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(restLoggingInterceptor)
            .addPathPatterns("/api/**");
    }
}
