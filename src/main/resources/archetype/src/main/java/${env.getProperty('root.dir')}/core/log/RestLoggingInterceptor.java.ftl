package ${env.getProperty('root.package')}.core.log;

import lombok.extern.slf4j.Slf4j;
import org.keycloak.KeycloakSecurityContext;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Interceptor que registra informacoes de log para todas as requisicoes HTTP da app.
 */
@Component
@Slf4j
public class RestLoggingInterceptor implements HandlerInterceptor {

    private final String MDC_UUID_KEY = "uuid";

    private long startTime;


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // Generate a UUID for the request
        long uniqueId = ThreadLocalRandom.current().nextLong();
        String hexString = Long.toHexString(uniqueId);
//        log.debug("KeycloakSecurityContext: {}", keycloakSecurityContext);
        Object token = request.getAttribute(KeycloakSecurityContext.class.getName());
        if (Objects.nonNull(token)) {
            KeycloakSecurityContext keycloakSecurityContext = (KeycloakSecurityContext) token;
            // Insere o username do usuário do AccessToke do Keycloak.
            String username = keycloakSecurityContext.getToken().getPreferredUsername();
            MDC.put("username", username);
        }

        startTime = System.currentTimeMillis();

        // Set the UUID in the MDC
        MDC.put(MDC_UUID_KEY, hexString);


        // Log the request URL, query params, and header value
        StringBuilder url = getURL(request);
        log.info("StartRequest: {}", url.toString());

        return true;
    }

    private static StringBuilder getURL(HttpServletRequest request) {
        StringBuilder sb = new StringBuilder();
        sb.append(request.getMethod()).append(" ").append(request.getRequestURL());
        String queryString = request.getQueryString();
        if (queryString != null) {
            sb.append("?").append(queryString);
        }
        return sb;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        // Do nothing
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception exception) throws Exception {
        long endTime = System.currentTimeMillis();
        long executionTime = endTime - startTime;
        StringBuilder url = getURL(request);
        log.info("EndRequest: {}, Execution time: {} ms", url.toString(), executionTime);
        // Remove the UUID from the MDC
        MDC.remove(MDC_UUID_KEY);
        MDC.remove("username");
    }
}
