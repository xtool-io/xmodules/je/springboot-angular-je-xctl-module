package ${env.getProperty('root.package')}.core.audit;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.RevisionEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@RevisionEntity(AppRevisionListener.class)
@Table( name = "AUD_REVINFO")
public class AppRevisionEntity extends DefaultRevisionEntity {

    private static final long serialVersionUID = 1L;

    private String modifiedBy;
}
