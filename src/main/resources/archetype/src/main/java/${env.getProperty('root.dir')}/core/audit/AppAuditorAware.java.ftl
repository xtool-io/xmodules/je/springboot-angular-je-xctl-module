package ${env.getProperty('root.package')}.core.audit;

import lombok.AllArgsConstructor;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.data.domain.AuditorAware;

import java.util.Objects;
import java.util.Optional;

@AllArgsConstructor
public class AppAuditorAware implements AuditorAware<String> {

    private final KeycloakSecurityContext keycloakSecurityContext;

    @Override
    public Optional<String> getCurrentAuditor() {
        if (Objects.nonNull(keycloakSecurityContext) && Objects.nonNull(keycloakSecurityContext.getToken().getEmail())) {
            return Optional.of(keycloakSecurityContext.getToken().getEmail());
        }
        return Optional.of("SYSTEM");
    }

}
