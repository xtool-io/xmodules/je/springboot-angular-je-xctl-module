package ${env.getProperty('root.package')}.core.security;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Classe que contém as informações de segurança do usuário.
 */
@Slf4j
@Component
@AllArgsConstructor
public class SecurityIdentity {

    private final KeycloakSecurityContext keycloakSecurityContext;

    private final Environment environment;

    /**
     * Retorna a lista de roles do usuário.
     *
     * @return Listagem de roles
     */
    public Set<String> getRoles() {
        String resource = environment.getRequiredProperty("keycloak.resource");
        if (Objects.nonNull(keycloakSecurityContext.getToken().getResourceAccess(resource))) {
            return Collections.unmodifiableSet(keycloakSecurityContext.getToken().getResourceAccess(resource).getRoles());
        }
        return Collections.emptySet();
    }

    /**
     * Verifica se o usuário possui a role correspondente.
     *
     * @param name
     * @return
     */
    public boolean hasRole(String name) {
        return this.getRoles().contains(name);
    }

    /**
     * Retorna o username do usuário.
     *
     * @return
     */
    public String getUsername() {
        return keycloakSecurityContext.getToken().getPreferredUsername();
    }

    /**
     * Retorna o email do usuário
     *
     * @return
     */
    public String getEmail() {
        return keycloakSecurityContext.getToken().getEmail();
    }

    /**
     * Retorna os grupos do usuário.
     *
     * @return
     */
    public Set<String> getGroups() {
        Object groupsRef = keycloakSecurityContext.getToken().getOtherClaims().get("groups");
        if (Objects.nonNull(groupsRef)) {
            List<String> groups = (List<String>) groupsRef;
            return Collections.unmodifiableSet(new HashSet<>(groups));
        }
        return Collections.emptySet();
    }

    public boolean hasGroup(String name) {
        return this.getGroups().contains(name);
    }

}
