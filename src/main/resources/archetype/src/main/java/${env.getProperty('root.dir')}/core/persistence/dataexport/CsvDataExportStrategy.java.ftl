package ${env.getProperty('root.package')}.core.persistence.dataexport;

import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;

import java.io.*;
import java.util.List;

public class CsvDataExportStrategy implements DataExportStrategy {
    @Override
    public DataExportResult exportCsv(List<Object[]> content, DataExportOptions dataExportOptions) throws IOException {
        CsvMapper mapper = new CsvMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        CsvSchema.Builder builder = CsvSchema.builder();
        dataExportOptions.getColumns().stream().forEachOrdered(builder::addColumn);
        CsvSchema schema = builder.setUseHeader(true).build();
        schema = schema.withHeader().withColumnSeparator('\t');
        ObjectWriter objectWriter = mapper.writer(schema);
        //-----
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        BufferedOutputStream bos = new BufferedOutputStream(baos);
        OutputStreamWriter writerOutputStream = new OutputStreamWriter(bos, "UTF-8");
        objectWriter.writeValue(writerOutputStream, content);
        DataExportResult exportResult = new DataExportResult();
        exportResult.setContent(new ByteArrayResource(baos.toByteArray()));
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + dataExportOptions.getFileName().concat(".csv") + "\"");
        exportResult.setHttpHeaders(httpHeaders);
        return exportResult;
    }
}
