package ${env.getProperty('root.package')}.core.audit;

import lombok.AllArgsConstructor;
import org.hibernate.envers.RevisionListener;
import org.keycloak.KeycloakSecurityContext;

import java.util.Optional;

@AllArgsConstructor
public class AppRevisionListener implements RevisionListener {

    private final KeycloakSecurityContext keycloakSecurityContext;
    @Override
    public void newRevision(Object revisionEntity) {
        String username = keycloakSecurityContext.getToken().getEmail();
        String currentUser = Optional.ofNullable(username)
                .orElse("Unknown-User");

        AppRevisionEntity appRevisionEntity = (AppRevisionEntity) revisionEntity;
        appRevisionEntity.setModifiedBy(currentUser);

    }
}
