package ${env.getProperty('root.package')}.app.config;

import com.github.tennaito.rsql.misc.ArgumentFormatException;
import com.github.tennaito.rsql.misc.DefaultArgumentParser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.regex.Pattern;

@Configuration
@Slf4j
public class RSQLArgumentParserConfig extends DefaultArgumentParser {
    @SuppressWarnings("unchecked")
    @Override
    public <T> T parse(String argument, Class<T> type) throws ArgumentFormatException, IllegalArgumentException {
        log.info("Argument: {}, ClassType: {}", argument, type.getSimpleName());
        if (type.equals(LocalDate.class)) return (T) toLocalDate(argument);
        if (type.equals(LocalDateTime.class)) return (T) LocalDateTime.parse(argument);
        if (type.equals(Short.class)) return (T) Short.valueOf(argument);
        if (type.equals(BigDecimal.class)) return (T) new BigDecimal(argument);
        if (isCpfPattern(argument)) return (T) argument.replaceAll("\\.", "").replaceAll("-", "");
        if (isTituloEleitor(argument)) return (T) argument.replaceAll("\\.", "").replaceAll("-", "");
        return super.parse(argument, type);
    }

    private LocalDate toLocalDate(String argument) {
        // Pattern de data em formato ISO-8601
        String pattern = "^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}$";
        // Verificação para quando a coluna de data do componente devextreme está sem nenhum registro
        // Neste caso o formato de data enviado não segue o padrão dd/MM/yyyy mas o ISO-8601
        // Sem o tratamento do bloco de código abaixo ocorre o erro de:
        // java.time.format.DateTimeParseException: Text '2023-06-01T03:00:00' could not be parsed, unparsed text found at index 10 when parse to LocalDate
        // Issue GitLab: https://git.tre-pa.jus.br/sds/apps/gestao-terceirizado/get/-/issues/116
        if (Pattern.matches(pattern, argument)) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
            LocalDateTime dateTime = LocalDateTime.parse(argument, formatter);
            return dateTime.toLocalDate();
        }
        return LocalDate.parse(argument);
    }

    // Caso o argument enviado tenha um pattern de CPF remove as máscaras das mesmas.
    private boolean isCpfPattern(String argument) {
        log.debug("Padrão de CPF encontrado: {}", argument);
        String pattern = "^\\d{3}\\.\\d{3}\\.\\d{3}-\\d{2}$";
        return Pattern.matches(pattern, argument.replaceAll("\\*", ""));
    }

    public boolean isTituloEleitor(String argument) {
        String pattern = "^\\d{2}\\.\\d{3}\\.\\d{3}-\\d{2}$";
        return Pattern.matches(pattern, argument.replaceAll("\\*", ""));
    }


}
