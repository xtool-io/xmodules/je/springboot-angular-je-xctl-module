<#assign schemaName=projectName?replace('_', '')?replace('-', '')?truncate(30, '') >
server:
  port: 8080
  servlet:
    context-path: /${projectName}
  error:
    include-message: always
spring:
  config:
    import: classpath:policy-enforcer.yml
  datasource:
    url: ${r'${SPRING_DATASOURCE_URL'}:jdbc:oracle:thin:@localhost:1521:ee}
    driver-class-name: oracle.jdbc.OracleDriver
    username: ${r'${SPRING_DATASOURCE_USERNAME'}:${schemaName}}
    password: ${r'${SPRING_DATASOURCE_PASSWORD'}:${schemaName}}
    sqlScriptEncoding: UTF-8
    initialization-mode: always
    hikari:
      auto-commit: false
  jackson:
    serialization:
      WRITE_DATES_AS_TIMESTAMPS: false
  jpa:
    hibernate:
      ddl-auto: update
      use-new-id-generator-mappings: true
    properties:
      connection:
        provider_disables_autocommit: true
      org:
        hibernate:
          envers:
            audit_table_prefix: AUD_
            audit_table_suffix:
      hibernate:
        format_sql: true
        generate_statistics: false
        use_sql_comments: true
    defer-datasource-initialization: true
logging:
  level:
    br:
      jus: ${r'${LOGGING_LEVEL'}:DEBUG}
    org:
      keycloak: ${r'${LOGGING_LEVEL'}:DEBUG}
      hibernate:
        SQL: ${r'${LOGGING_LEVEL'}:DEBUG}
        type:
          descriptor:
            sql: TRACE
      springframework:
        jdbc:
          core:
            JdbcTemplate: ${r'${LOGGING_LEVEL'}:DEBUG}
  pattern:
    console: "%clr(%d{dd/MM/yyyy HH:mm:ss.SSS}){faint} %clr(%X{uuid}/%X{username}){yellow} %clr(%-5level) %clr(%logger{36}){cyan} - %msg%n"
keycloak:
  auth-server-url: ${r'${KEYCLOAK_AUTH_SERVER_URL'}:http://localhost:8085/auth}
  bearer-only: true
  config:
    base-url: ${r'${KEYCLOAK_CONFIG_BASE_URL'}:http://localhost:4200/}
    valid-redirect-url: ${r'${KEYCLOAK_CONFIG_VALID_REDIRECT_URL'}:http://localhost:4200/*}
  credentials:
    secret: ${r'${KEYCLOAK_CREDENTIALS_SECRET'}:f541e548-ff50-4c97-928d-f296fcd7be9c}
  realm: TRE-PA
  resource: ${projectName}-backend
  ssl-required: external
  use-resource-role-mappings: true
  policy-enforcer-config:
    enforcement-mode: ENFORCING
    lazy-load-paths: false
    http-method-as-scope: false
  security-constraints:
    - authRoles: ["*"]
      security-collections:
        - patterns: [ "/api/*" ]
<#--info:-->
<#--  app:-->
<#--    name: ${projectName}-->
management:
  endpoints:
    web:
      exposure:
        include: "health,info,prometheus,auditevents"
