---
rolePolicy:
  clientId: ${projectName}-backend
  name: "admin:role:policy"
  description: Role ADMIN
  client:
    name: "${projectName}-backend"
    roles: ["ADMIN"]
