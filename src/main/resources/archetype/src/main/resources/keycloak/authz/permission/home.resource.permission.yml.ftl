---
resourcePermission:
  clientId: ${projectName}-backend
  name: "home:permission"
  description: Home Permission
  resources: ["home:resource"]
  policies: ["auth:js:policy"]
