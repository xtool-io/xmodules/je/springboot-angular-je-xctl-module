policy-enforcer-config:
  paths:
    - path: /
      enforcement-mode: DISABLED
    - path: /*.js
      enforcement-mode: DISABLED
    - path: /*.css
      enforcement-mode: DISABLED
    - path: /*.html
      enforcement-mode: DISABLED
    - path: /*.ttf
      enforcement-mode: DISABLED
    - path: /*.woff2
      enforcement-mode: DISABLED
    - path: /*.woff
      enforcement-mode: DISABLED
    - path: /assets/*
      enforcement-mode: DISABLED
