<#assign schemaName=projectName?replace('_', '')?replace('-', '')?truncate(30, '') >
connect system/oracle
alter session set "_ORACLE_SCRIPT"=true;
--**********************************
--Esquema ${schemaName}
--**********************************

create tablespace ${schemaName} datafile '/opt/oracle/oradata/${schemaName}01.dbf' size 100M online;
create tablespace idx_${schemaName} datafile '/opt/oracle/oradata/idx_${schemaName}01.dbf' size 100M;
create user ${schemaName} identified by ${schemaName} default tablespace ${schemaName} temporary tablespace temp;
alter user ${schemaName} quota unlimited on ${schemaName};
grant resource to ${schemaName};
grant connect to ${schemaName};
grant create view to ${schemaName};
grant create procedure to ${schemaName};
grant create materialized view to ${schemaName};
alter user ${schemaName} default role connect, resource;
exit;
