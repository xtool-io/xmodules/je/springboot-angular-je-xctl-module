/* You can add global styles to this file, and also import other style files */
@import '~@angular/material/prebuilt-themes/indigo-pink.css';


.detail-subheader {
  cursor: default;
  margin-bottom: 8px;
}

.detail-column {
  padding: 4px;
  margin-top: -4px;
  display: flex;

  div {
    p {
      cursor: default;
      margin-top: -4px;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }
  }
}

.detail-label-column {
  padding-right: 8px;
  text-align: end;
  font-weight: bold;
}

.detail-value-column {
  flex: 1 1 0%;
}

.dx-drawer-content {
  background-color: #f2f2f2;
}

.dx-field-label {
  width: auto !important;
}

.dx-field-value {
  width: auto !important;
}

.dx-item {
  .dx-field-item-has-group {
    margin-top: 25px;
  }
}

.content-block {
  margin-top: 0px;
}


.form-block {
  min-height: 250px;

  .form-title {
    font-size: 22px;
    font-weight: 500;
  }
}

.dx-form {
  .dx-texteditor-label {
    height: auto;
  }

  .dx-texteditor-with-label .dx-label {
    line-height: 1;
  }
}

.plain-styled-form {
  .dx-layout-manager .dx-field-item:not(.dx-first-col) {
    padding-left: 0;
  }

  .dx-field-item {
    padding: 0;

    &.contact-fields-group {
      padding: 15px 0;
    }

    .status-indicator,
    .status {
      line-height: 18px;
      font-size: 13px;
    }
  }

  &.view-mode {
    .accent {
      .dx-texteditor.form-editor .form-editor-input {
        //color: $accent-color;
      }
    }

    .dx-texteditor,
    .dx-texteditor.form-editor, {
      pointer-events: none;

      .form-editor-input {
        //color: $texteditor-edit-color;
      }

      &,
      & > * {
        background-color: transparent;
      }

      &,
      & *,
      &::after {
        border-color: transparent;
        opacity: 1;
      }
    }
  }
}

.list-header-button {
  border: 0px;
  font-size: 14px;
  opacity: 100 !important;
  margin-top: 15px;

  .dx-button-content {
    padding: 0px;
  }

  span {
    font-weight: normal !important;
    color: black !important;
    text-transform: none;
  }
}

.mat-mdc-dialog-content {
  overflow: hidden !important;

  .dx-item {
    margin-bottom: 15px;
  }
}

.mat-mdc-dialog-actions {
  margin-left: 16px !important;

  dx-button {
    margin-right: 5px !important;
  }
}
