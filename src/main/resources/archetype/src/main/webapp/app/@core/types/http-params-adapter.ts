import {HttpParams} from "@angular/common/http";
import {LoadOptions, SortDescriptor} from "devextreme/data";
import {toFilterable} from "../udf/udf-parser";
import {UdfContains} from "../udf/predicate/udf-contains";
import {UdfFilterable} from "../udf/udf-filterable";

/**
 * Classe Adapter para montagem do HttpParams a partir do LoadOptions
 */
export class HttpParamsAdapter {

  constructor(private options: LoadOptions) {
  }

  httpParams(): HttpParams {
    const
      size = this.options.take,
      page = this.options.skip ? this.options.skip / (size || 1) : 0,
      sort = this.options.sort || [];
    let params = new HttpParams()
      .append('page', `${page}`)
      .append('size', `${size}`);
    if (this.hasFilter(this.options)) params = params.append("q", this.prepareFilter(this.options))
    params = this.prepareSort(sort, params);
    if (this.hasSummary(this.options)) params = params.append("s", this.prepareSummary(this.options))
    return params;
  }

  private prepareSort(sort: SortDescriptor<any> | SortDescriptor<any>[], params: HttpParams): HttpParams {
    if (Array.isArray(sort)) {
      for (let i = 0; i < sort.length; i++) {
        // @ts-ignore
        params = params.append('sort', `${sort[i].selector},${sort[i].desc ? 'desc' : 'asc'}`);
      }
      return params;
    }

  }

  protected hasFilter(options: LoadOptions) {
    return (options.searchExpr && options.searchOperation && options.searchValue) || options.filter
  }

  protected prepareFilter(options: LoadOptions): string {
    let udfFilterable = options.filter ? toFilterable(options.filter) : new UdfFilterable();
    if (options.searchExpr && options.searchOperation && options.searchValue) {
      if (options.searchOperation === 'contains') {
        let searchPredicate = new UdfContains(options.searchExpr as string, options.searchValue);
        udfFilterable.predicates.push(searchPredicate);
      }
    }
    return udfFilterable.toQueryParam();
  }


  protected hasSummary(options: LoadOptions) {
    return options.totalSummary;
  }

  protected prepareSummary(options: LoadOptions): string {
    // @ts-ignore
    const summaries: { selector: string, summaryType: string }[] = options.totalSummary;
    return summaries
      .map(summary => `${summary.selector},${summary.summaryType}`)
      .join(";");
  }
}
