import { Location } from "@angular/common";
import { Injector } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { DxDataGridComponent } from "devextreme-angular/ui/data-grid";
import DataSource from "devextreme/data/data_source";
import { confirm } from "devextreme/ui/dialog";
import notify from "devextreme/ui/notify";
import { Observable } from "rxjs";
import { take } from "rxjs/operators";
import { StandardNgConfig } from "./standard-ng-config";

export abstract class StandardNgDataGridComponent<T, ID> {

  readonly abstract dataGrid: DxDataGridComponent;

  dataSource: DataSource;

  protected clearFilterButton: any;

  protected titleButton: any

  // pageTitle: string

  // Quantidade de recursos totais sem paginação na base de dados (levando em consideração a filtragem aplicada a lista)
  totalResources = 0;

  abstract config: StandardNgConfig;

  // private title: Title;

  private activatedRoute: ActivatedRoute;

  private router: Router;

  private location: Location;

  protected constructor(
    protected injector: Injector,
  ) {
    // this.title = injector.get(Title);
    this.activatedRoute = injector.get(ActivatedRoute);
    this.router = injector.get(Router);
    this.location = injector.get(Location);
    // this.pageTitle = this.title.getTitle();
    this.dataSource = this.activatedRoute.snapshot.data['dataSource']
    this.dataSource.on({
      'changed': () => {
        this.totalResources = this.dataSource.totalCount();
      }
    });
  }


  /**
   *
   */
  create() {
    this.router.navigate([this.config.routePath, 'new']);
  }

  /**
   *
   * @param id
   */
  detail(id: number) {
    this.router.navigate([this.config.routePath, 'detail', id]);
  }


  async onContentReady() {
    this.clearFilterButton.option('visible', this.hasFilter());
  }

  private hasFilter(): boolean {
    return (
      this.dataSource.isLoaded() &&
      this.dataGrid.instance.getCombinedFilter() &&
      this.dataGrid.instance.getCombinedFilter().length > 0
    );
  }

  protected clearFilterDataGrid() {
    this.dataGrid.instance.clearFilter('header');
    this.dataGrid.instance.clearFilter('row');
    this.dataGrid.instance.clearFilter('search');
  }

  confirmDelete(id: ID) {
    confirm(
      this.config.confirmDeleteMessage,
      'Confirme a exclusão'
    ).then(result => {
      if (result) {
        this.doDelete(id)
          .pipe(take(1))
          .subscribe(() => {
            notify(
              this.config.onSuccessDelete,
              'success',
              3000
            );
            this.dataGrid.instance.refresh();
          });
      }
    });
  }

  protected confirmExport() {
    if (this.totalResources == 0) {
      notify(
        "Não há dados para exportação!",
        "error",
        3000
      )
      return;
    }
    confirm(
      `${this.config.confirmExportMessage} <br><b>${this.totalResources} registros serão exportado(s).</b>`,
      'Confirme a exportação'
    ).then(result => {
      if (result) {
        this.doExportCsv()
      }
    });
  }

  protected abstract doDelete(id: ID): Observable<void>;

  protected abstract doExportCsv();

}
