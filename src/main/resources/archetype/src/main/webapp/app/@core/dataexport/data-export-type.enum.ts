export enum DataExportType {
  CSV = "CSV",
  PDF = "PDF",
  XLSX = "XLSX",
  JSON = "JSON"
}
