<ng-container>
    <app-side-nav-outer-toolbar title="${projectName}">
        <router-outlet></router-outlet>
    </app-side-nav-outer-toolbar>
</ng-container>
