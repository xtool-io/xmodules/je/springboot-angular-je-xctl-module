import {ActivatedRoute, Router} from "@angular/router";
import {Location} from "@angular/common";
import {Title} from "@angular/platform-browser";
import {Subscription} from "rxjs";
import {take} from "rxjs/operators";
import notify from "devextreme/ui/notify";
import {Injector} from "@angular/core";
import {StandardNgService} from "./standard-ng-service";
import {confirm} from "devextreme/ui/dialog";


export abstract class StandardNgDetailComponent<T, ID> {

  formData: T;

  private loadedData: T;

  saveSub: Subscription;

  pageTitle: string;

  isEditing = false;

  abstract config: {
    idAtt: string,
    detailTitle: string,
    editTitle: string,
    createTitle: string,
    onSuccessEditMessage: string,
    onSuccessCreateMessage: string,
    onSuccessDeleteMessage: string,
    confirmDeleteMessage: string,
  };

  private title: Title;

  private activatedRoute: ActivatedRoute;

  private router: Router;

  private location: Location;

  protected constructor(
    protected injector: Injector,
    private service: StandardNgService
  ) {
    this.title = injector.get(Title);
    this.activatedRoute = injector.get(ActivatedRoute);
    this.router = injector.get(Router);
    this.location = injector.get(Location);
    this.formData = this.loadedData = {...this.activatedRoute.snapshot.data['formData']};
  }


  back() {
    this.location.back();
  };

  save(e) {
    if (this.isInvalidFormData(e)) return;
    const message = this.isCreateMode() ? this.config.onSuccessCreateMessage : this.config.onSuccessEditMessage;
    return this.service.save(this.formData)
      .subscribe(resource => {
          notify(
            {message},
            'success',
            5000);
          this.isEditing = false;
          this.formData = this.loadedData = {...resource}
          this.setTitle(this.config.detailTitle)
        }
      );
  }


  edit() {
    this.isEditing = true;
    this.setTitle(this.isCreateMode() ? this.config.createTitle : this.config.editTitle);
  }

  delete() {
    const id = this.formData[this.config.idAtt];
    confirm(
      this.config.confirmDeleteMessage,
      'Confirme a exclusão'
    ).then(result => {
      if (result) {
        this.service.delete(id)
          .pipe(take(1))
          .subscribe(() => {
            notify(
              this.config.onSuccessDeleteMessage,
              'success',
              3000
            );
            this.back()
          });
      }
    });
  }

  cancel() {
    if (this.isCreateMode()) this.back();
    this.formData = {...this.loadedData};
    this.isEditing = false;
    this.setTitle(this.config.detailTitle);
  }

  /**
   * Retorna true case seja a criação de um novo registro.
   * @private
   */
  isCreateMode() {
    return !this.formData[this.config.idAtt]
  }


  protected load() {
    if (this.isCreateMode()) {
      this.isEditing = true;
      this.setTitle(this.config.createTitle)
      return;
    }
    this.isEditing = false;
    this.setTitle(this.config.detailTitle);
  }

  private setTitle(title: string) {
    this.pageTitle = title;
    this.title.setTitle(title);
  }

  private isInvalidFormData(e) {
    if (!e.validationGroup.validate().isValid) {
      notify("O formulário possui erros de validação.", 'error', 3000)
      return true;
    }
    return false;
  }


}
