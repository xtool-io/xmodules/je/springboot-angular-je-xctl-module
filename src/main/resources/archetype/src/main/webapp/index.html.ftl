<!doctype html>
<html lang="pt-br">
<head>
  <meta charset="utf-8">
  <title>${projectName}</title>
  <base href="/">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href="generators/app/archetypet-angular/template/${projectName}-frontend/src/favicon.ico">
</head>
<body class="dx-viewport">
  <app-root></app-root>
</body>
</html>
