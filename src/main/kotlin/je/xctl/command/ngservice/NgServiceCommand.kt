package je.xctl.command.ngservice

import org.springframework.stereotype.Component
import picocli.CommandLine.Command
import xtool.core.command.XctlCommand

@Component
@Command(
    name = "ng-service",
    mixinStandardHelpOptions = true,
    description = ["Comando para manipulação de services Angular."]
)
class NgServiceCommand() : XctlCommand() {
    override fun run() {
    }
}
