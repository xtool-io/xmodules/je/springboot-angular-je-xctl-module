package je.xctl.command.repository

import org.springframework.stereotype.Component
import picocli.CommandLine.Command
import xtool.core.command.XctlCommand

@Component
@Command(
    name = "repository",
    subcommands = [RepositoryGenerateCommand::class],
    mixinStandardHelpOptions = true,
    description = ["Comando para manipulação de interfaces de repositório Spring Boot."]
)
class RepositoryCommand : XctlCommand() {
    override fun run() {
    }
}
