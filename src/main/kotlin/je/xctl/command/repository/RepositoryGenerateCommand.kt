package je.xctl.command.repository

import je.xctl.service.RepositoryGenerateFromEntityService
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import picocli.CommandLine
import picocli.CommandLine.Command
import xtool.core.command.XctlSubCommand
import xtool.xdepspringboot.command.converter.EntityConverter
import xtool.xdepspringboot.command.provider.EntityValueProvider
import xtool.xdepspringboot.model.Entity

@Component
@Command(
    name = "generate",
    mixinStandardHelpOptions = true,
    description = ["Comando para a geração de interface de repositório Spring Boot."]
)
class RepositoryGenerateCommand(
    private val repositoryGenerateFromEntityService: RepositoryGenerateFromEntityService,
) : XctlSubCommand() {

    private val log = LoggerFactory.getLogger(RepositoryGenerateCommand::class.java)

    /**
     * Option para o caminho com diagrama de classe PlantUML.
     */
    @CommandLine.Option(
        names = ["--entity"],
        description = ["Entidade JPA."],
        completionCandidates = EntityValueProvider::class,
        converter = [EntityConverter::class],
        required = false
    )
    private lateinit var entity: Entity
    override fun run() {
        repositoryGenerateFromEntityService.exec(entity)
    }

}
