package je.xctl.command.entity

import org.springframework.stereotype.Component
import picocli.CommandLine.Command
import xtool.core.command.XctlCommand

/**
 * Comando raiz
 */
@Component
@Command(
    name = "entity",
    mixinStandardHelpOptions = true,
    subcommands = [EntityGenerateCommand::class],
    description = ["Comando para manipulação de entidades JPA."]
)
class EntityCommand : XctlCommand() {
    override fun run() {
    }
}
