package je.xctl.command.entity

import je.xctl.service.EntityGenerateService
import org.slf4j.LoggerFactory
import org.springframework.core.env.Environment
import org.springframework.stereotype.Component
import picocli.CommandLine
import picocli.CommandLine.Command
import xtool.core.command.XctlSubCommand
import xtool.core.prompt.components.History
import xtool.core.prompt.core.ValidationContext
import xtool.core.tasks.FileSystemTask
import xtool.core.tasks.PromptTask
import xtool.xdepplantuml.command.converter.PlantClassDiagramConverter
import xtool.xdepplantuml.command.provider.PlantClassDiagramValueProvider
import xtool.xdepplantuml.model.PlantClassDiagram

/**
 * Comando responsável pela geração das entidades JPA.
 */
@Component
@Command(
    name = "generate",
    description = ["Comando para a geração de entidades JPA."],
    mixinStandardHelpOptions = true
)
class EntityGenerateCommand(
    private val entityGenerateService: EntityGenerateService,
) : XctlSubCommand() {

    private val log = LoggerFactory.getLogger(EntityGenerateCommand::class.java)

    /**
     * Option para o caminho com diagrama de classe PlantUML.
     */
    @CommandLine.Option(
        names = ["--from-diagram"],
        description = ["Caminho para geração baseada em um diagrama de classe PlantUML."],
        completionCandidates = PlantClassDiagramValueProvider::class,
        converter = [PlantClassDiagramConverter::class]
    )
    private lateinit var plantClassDiagram: PlantClassDiagram

    override fun run() {
        entityGenerateService.exec(plantClassDiagram)
    }
}
