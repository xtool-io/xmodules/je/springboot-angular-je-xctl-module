package je.xctl.command.ngformdialog

import org.springframework.stereotype.Component
import picocli.CommandLine.Command
import xtool.core.command.XctlCommand

@Component
@Command(
    name = "ngformdialog",
    mixinStandardHelpOptions = true,
    description = ["Comando para manipulação de formulário do tipo dialog."])
class NgFormDialogCommand: XctlCommand() {
    override fun run() {
//        TODO("Not yet implemented")
    }
}
