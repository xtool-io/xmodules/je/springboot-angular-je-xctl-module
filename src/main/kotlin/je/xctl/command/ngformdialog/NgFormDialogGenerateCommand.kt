package je.xctl.command.ngformdialog

import org.springframework.stereotype.Component
import picocli.CommandLine.Command
import xtool.core.command.XctlSubCommand

@Component
@Command(
    name = "generate",
    mixinStandardHelpOptions = true,
    description = ["Comando para a geração de formulário com dialog em Angular."]
)
class NgFormDialogGenerateCommand : XctlSubCommand() {
    override fun run() {
//        TODO("Not yet implemented")
    }
}
