package je.xctl.command.rest

import org.springframework.stereotype.Component
import picocli.CommandLine.Command
import xtool.core.command.XctlCommand

@Component
@Command(
    name = "rest",
    subcommands = [RestGenerateCommand::class],
    mixinStandardHelpOptions = true,
    description = ["Comando para manipulação de classes Rest Spring Boot."]
)
class RestCommand : XctlCommand() {
    override fun run() {
    }
}
