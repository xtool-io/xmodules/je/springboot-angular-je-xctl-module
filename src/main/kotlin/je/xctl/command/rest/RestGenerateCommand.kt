package je.xctl.command.rest

import je.xctl.service.RestGenerateFromEntityService
import org.springframework.stereotype.Component
import picocli.CommandLine
import picocli.CommandLine.Command
import xtool.core.command.XctlSubCommand
import xtool.xdepspringboot.command.converter.EntityConverter
import xtool.xdepspringboot.command.provider.EntityValueProvider
import xtool.xdepspringboot.model.Entity

@Component
@Command(
    name = "generate",
    mixinStandardHelpOptions = true,
    description = ["Comando para a geração de uma classe de Rest Spring Boot."]
)
class RestGenerateCommand(
    private val restGenerateFromEntityService: RestGenerateFromEntityService,
) : XctlSubCommand() {

    /**
     * Option para o caminho com diagrama de classe PlantUML.
     */
    @CommandLine.Option(
        names = ["--from-entity"],
        description = ["Caminho a entidade JPA."],
        completionCandidates = EntityValueProvider::class,
        converter = [EntityConverter::class],
        required = false
    )
    private lateinit var entity: Entity
    override fun run() {
        restGenerateFromEntityService.exec(entity)
    }

}
