package je.xctl.command.ngformcard

import org.springframework.stereotype.Component
import picocli.CommandLine.Command
import xtool.core.command.XctlSubCommand

@Component
@Command(
    name = "generate",
    mixinStandardHelpOptions = true,
    description = ["Comando para a geração de formulário com card em Angular."]
)
class NgFormCardGenerateCommand : XctlSubCommand() {
    override fun run() {
//        TODO("Not yet implemented")
    }
}
