package je.xctl.command.ngformcard

import org.springframework.stereotype.Component
import picocli.CommandLine.Command
import xtool.core.command.XctlCommand

@Component
@Command(
    name = "ngformcard",
    mixinStandardHelpOptions = true,
    description = ["Comando para manipulação de formulário do tipo card."])
class NgFormCardCommand: XctlCommand() {
    override fun run() {
//        TODO("Not yet implemented")
    }
}
