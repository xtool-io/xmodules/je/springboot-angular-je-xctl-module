package je.xctl.command.ngpage

import org.springframework.stereotype.Component
import picocli.CommandLine.Command
import xtool.core.command.XctlCommand

@Component
@Command(
    name = "ng-page",
    subcommands = [NgPageGenerateCommand::class],
    mixinStandardHelpOptions = true,
    description = ["Comando para manipulação de páginas Angular."]
)
class NgPageCommand : XctlCommand() {
    override fun run() {
    }
}
