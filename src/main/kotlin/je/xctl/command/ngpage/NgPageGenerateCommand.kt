package je.xctl.command.ngpage

import je.xctl.service.NgPageGenerateFromEntityService
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import picocli.CommandLine
import picocli.CommandLine.Command
import xtool.core.command.XctlSubCommand
import xtool.xdepspringboot.command.converter.EntityConverter
import xtool.xdepspringboot.command.provider.EntityValueProvider
import xtool.xdepspringboot.model.Entity

@Component
@Command(
    name = "generate",
    mixinStandardHelpOptions = true,
    description = ["Comando para a geração de página Angular."]
)
class NgPageGenerateCommand(
    private val ngPageGenerateFromEntityService: NgPageGenerateFromEntityService,
) : XctlSubCommand() {

    private val log = LoggerFactory.getLogger(NgPageGenerateCommand::class.java)

    /**
     * Entidade JPA.
     */
    @CommandLine.Option(
        names = ["--from-entity"],
        description = ["Entidade JPA."],
        completionCandidates = EntityValueProvider::class,
        converter = [EntityConverter::class],
        required = false
    )
    private lateinit var entity: Entity

    override fun run() {
        ngPageGenerateFromEntityService.exec(entity)
    }

}
