package je.xctl.command.ngdatagrid

import org.springframework.core.env.Environment
import org.springframework.stereotype.Component
import picocli.CommandLine
import picocli.CommandLine.Command
import xtool.core.command.XctlSubCommand
import xtool.core.context.WorkspaceContext
import xtool.core.tasks.FileSystemTask
import xtool.xdepng.command.converter.NgPageModuleConverter
import xtool.xdepng.command.provider.NgPageModuleValueProvider
import xtool.xdepng.model.NgPageModule
import xtool.xdepspringboot.command.converter.EntityConverter
import xtool.xdepspringboot.command.provider.EntityValueProvider
import xtool.xdepspringboot.model.Entity

@Component
@Command(name = "generate")
class NgDataGridGenerateCommand(
    private val fsTask: FileSystemTask,
    private val env: Environment,
    private val workspaceContext: WorkspaceContext,
) : XctlSubCommand() {

    /**
     * Entidade JPA.
     */
    @CommandLine.Option(
        names = ["--from-entity"],
        description = ["Entidade JPA."],
        completionCandidates = EntityValueProvider::class,
        converter = [EntityConverter::class],
        required = false
    )
    private lateinit var entity: Entity

    @CommandLine.Option(
        names = ["--page-module"],
        description = ["Módulo da página."],
        completionCandidates = NgPageModuleValueProvider::class,
        converter = [NgPageModuleConverter::class],
        required = false
    )
    private lateinit var pageModule: NgPageModule

    override fun run() {
        generateNgList()
    }

    fun generateNgList() {
        fsTask.copyFiles(
            classpathSource = "templates/ngdatagrid",
            vars = mapOf(
                "ngPagePath" to workspaceContext.path.relativize(pageModule.path.parent),
                "ngListName" to entity.resourceName.plus("-list"),
                "entity" to entity
            )
        )
    }
}
