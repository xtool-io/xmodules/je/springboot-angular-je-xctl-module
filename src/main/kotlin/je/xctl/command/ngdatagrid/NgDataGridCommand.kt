package je.xctl.command.ngdatagrid

import org.springframework.stereotype.Component
import picocli.CommandLine.Command
import xtool.core.command.XctlCommand

@Component
@Command(
    name = "ng-datagrid",
    mixinStandardHelpOptions = true,
    description = ["Comando para manipulação de componentes Angular de listagem com DataGrid."]
)
class NgDataGridCommand : XctlCommand() {
    override fun run() {
    }
}
