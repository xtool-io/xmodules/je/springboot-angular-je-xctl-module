package je.xctl.utils

import strman.Strman

fun String.toKebabCase(): String = Strman.toKebabCase(this)

fun String.toStudlyCase(): String = Strman.toStudlyCase(this)
