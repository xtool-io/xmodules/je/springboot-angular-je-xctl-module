package je.xctl.config

import org.fusesource.jansi.Ansi
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import xtool.core.context.WorkspaceContext
import xtool.core.event.AppGenerateEvent
import xtool.core.tasks.FileSystemTask
import xtool.core.tasks.GitTask
import xtool.xdepng.tasks.NodeTask
import java.nio.file.attribute.PosixFilePermission

/**
 * Classe que implementa ações no evento de geração.
 */
@Primary
@Configuration
class AppGenerateEventConfiguration(
    private val workspaceContext: WorkspaceContext,
    private val gitTask: GitTask,
    private val fsTask: FileSystemTask,
    private val nodeTask: NodeTask,
) : AppGenerateEvent {

    private val log = LoggerFactory.getLogger(AppGenerateEventConfiguration::class.java)

    /**
     * Método chamado antes da geração da aplicação
     */
    override fun onBeforeInitialize() {
        log.info(Ansi.ansi().render("Iniciando geração da aplicação @|cyan,bold ${workspaceContext.basename} |@...").reset().toString())
    }

    /**
     * Método chamado após a geração da aplicação
     */
    override fun onAfterInitialize() {
        nodeTask.npm(
            args = listOf("i"),
            inheritIO = true)
        fsTask.setPermission(
            workspaceContext.path.resolve("kurl.sh"),
            setOf(
                PosixFilePermission.OWNER_READ,
                PosixFilePermission.OWNER_EXECUTE,
                PosixFilePermission.OWNER_WRITE,
                PosixFilePermission.GROUP_EXECUTE,
                PosixFilePermission.OTHERS_EXECUTE
            )
        )
        gitTask.init()
        gitTask.add()
        gitTask.commit("First Commit")
        log.info(Ansi.ansi().render("Aplicação @|cyan,bold ${workspaceContext.basename}|@ gerada com sucesso!").reset().toString())
        log.info(Ansi.ansi().render("Leia o README.md para maiores instruções.").reset().toString())
    }
}
