package je.xctl.freemarker

import org.springframework.context.annotation.Configuration
import xtool.core.template.TemplateSharedVariableConfigurer
import xtool.xdepplantuml.tasks.PlantTask

@Configuration
class TemplateSharedVariableConfiguration(
    private val plantTask: PlantTask,
) : TemplateSharedVariableConfigurer {
    override fun vars(): Map<String, Any> {
        return mapOf(
            "plantTask" to plantTask
        )
    }
}
