package je.xctl.service

import org.slf4j.LoggerFactory
import org.springframework.core.env.Environment
import org.springframework.stereotype.Service
import xtool.core.context.WorkspaceContext
import xtool.core.tasks.FileSystemTask
import xtool.xdepng.model.*
import xtool.xdepng.tasks.NgTask
import xtool.xdepng.tasks.NodeTask
import xtool.xdepspringboot.model.Entity
import java.nio.file.Paths

@Service
class NgPageGenerateFromEntityService(
    private val env: Environment,
    private val fsTask: FileSystemTask,
    private val ngTask: NgTask,
    private val nodeTask: NodeTask,
    private val workspaceContext: WorkspaceContext,
    private val ngServiceGenerateFromEntityService: NgServiceGenerateFromEntityService,
    private val ngDataGridGenerateFromEntityService: NgDataGridGenerateFromEntityService,
    private val ngFormDialogGenerateService: NgFormDialogGenerateService,
    private val ngFormCardGenerateService: NgFormCardGenerateService,
) {

    private val log = LoggerFactory.getLogger(NgDataGridGenerateFromEntityService::class.java)

    fun exec(entity: Entity) {
        generateNgService(entity)
        generateNgPage(entity)
        generateNgList(entity)
        generateNgDetail(entity)
        updateRoute(entity)
        updateNavigation(entity)
        entity.toOneEntities
            .filter { it.hasAnnotation("SettingEntity") }
            .forEach {
                generateNgService(it)
                generateNgPage(it)
                generateNgList(it)
                generateNgDetail(it)
                updateRoute(it)
                updateNavigation(it)
            }
        entity.toManyEntities
            .filter { it.hasAnnotation("SettingEntity") }
            .forEach {
                generateNgService(it)
                generateNgPage(it)
                generateNgList(it)
                generateNgDetail(it)
                updateRoute(it)
                updateNavigation(it)
            }
        format(entity)
    }

    private fun generateNgDetail(entity: Entity) {
        when (entity.hasAnnotation("SettingEntity")) {
            true -> ngFormDialogGenerateService.exec(entity)
            else -> ngFormCardGenerateService.exec(entity)
        }
    }

    private fun generateNgList(entity: Entity) {
        ngDataGridGenerateFromEntityService.exec(entity)
    }

    private fun generateNgService(entity: Entity) {
        ngServiceGenerateFromEntityService.exec(entity)
    }

    private fun generateNgPage(entity: Entity) {
        val pageName = entity.resourceName.plus("-page")
        val pageClassName = entity.name.plus("Page")
        val classpathSource = when (entity.hasAnnotation("SettingEntity")) {
            true -> "templates/ngpage/setting"
            else -> "templates/ngpage/default"
        }
        val ngPagePath = when (entity.hasAnnotation("SettingEntity")) {
            true -> env.getRequiredProperty("ng-page.path").plus("/@settings")
            else -> env.getRequiredProperty("ng-page.path")
        }

        fsTask.copyFiles(
            classpathSource = classpathSource,
            vars = mapOf(
                "ngPagePath" to ngPagePath,
                "pageName" to pageName,
                "pageClassName" to pageClassName,
                "entity" to entity
            )
        )
    }

    private fun updateNavigation(entity: Entity) {
        val appPath = workspaceContext.path.resolve(Paths.get(env.getRequiredProperty("ng-app.path")))
        val appNavigation = NgNavigation(appPath.resolve("app-navigation.ts"))
        val icon = when (entity.hasAnnotation("SettingEntity")) {
            true -> ""
            else -> "file"
        }
        val pageNav = NgNavigationItem(
            path = entity.resourceName,
            text = entity.name,
            icon = icon,
            data = mapOf(
                "permissions" to mapOf(
                    "only" to "${entity.resourceName}:resource:view"
                )
            ),
        )
        when (entity.hasAnnotation("SettingEntity")) {
            true -> ngTask.addNavigation(appNavigation, pageNav, "Configurações")
            else -> ngTask.addNavigation(appNavigation, pageNav, "")
        }
//        ngTask.addNavigation(appNavigation, pageNav)
        ngTask.format(appNavigation)
    }

    private fun updateRoute(entity: Entity) {
        val pageName = entity.resourceName.plus("-page")
        val appPath = workspaceContext.path.resolve(Paths.get(env.getRequiredProperty("ng-app.path")))
        val appRoutingModule = NgRoutingModule(appPath.resolve("app-routing.module.ts"))
        val pageModulePath = when (entity.hasAnnotation("SettingEntity")) {
            true -> workspaceContext.path.resolve(Paths.get(env.getRequiredProperty("ng-page.path")).resolve("@settings/${pageName}/${pageName}.module.ts"))
            else -> workspaceContext.path.resolve(Paths.get(env.getRequiredProperty("ng-page.path")).resolve("${pageName}/${pageName}.module.ts"))
        }
        val pageModule = NgPageModule(pageModulePath)
        log.debug("updateRoute(entity={})", entity.name)
        log.debug("  > pageName={}", pageName)
        log.debug("  > appPath={}", appPath)
        log.debug("  > appRoutingModule={}", appRoutingModule)
        log.debug("  > pageModulePath={}", pageModulePath)
        log.debug("  > pageModule={}", pageModule)
        val ngRoute = NgRoute(
            path = entity.resourceName,
            loadChildren = NgRoute.loadChildenFrom(appRoutingModule, pageModule),
            canActivate = listOf("NgxPermissionsGuard"),
            data = mapOf(
                "permissions" to mapOf(
                    "only" to "${entity.resourceName}:resource:view"
                )
            )
        )
        ngTask.addRoute(appRoutingModule, ngRoute)
        ngTask.format(appRoutingModule)
    }

    private fun format(entity: Entity) {
        val pageName = entity.resourceName.plus("-page")
        val ngPagePath = when (entity.hasAnnotation("SettingEntity")) {
            true -> env.getRequiredProperty("ng-page.path").plus("/@settings")
            else -> env.getRequiredProperty("ng-page.path")
        }
        log.info("Formatando artefatos do diretório {}", ngPagePath.plus("/$pageName"))
        nodeTask.npx(listOf("prettier --write ${ngPagePath.plus("/$pageName")} > /dev/null"))
        nodeTask.npx(listOf("prettier --write ${env.getRequiredProperty("ng-service.path")} > /dev/null"))
        nodeTask.npx(listOf("prettier --write ${env.getRequiredProperty("ng-domain.path")} > /dev/null"))
    }


}
