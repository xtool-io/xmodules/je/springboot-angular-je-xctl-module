package je.xctl.service

import org.slf4j.LoggerFactory
import org.springframework.core.env.Environment
import org.springframework.stereotype.Service
import xtool.core.tasks.FileSystemTask
import xtool.xdepng.tasks.NgTask
import xtool.xdepng.tasks.NodeTask
import xtool.xdepplantuml.model.PlantClassDiagram

@Service
class EntityGenerateService(
    private val fsTask: FileSystemTask,
    private val env: Environment,
    private val nodeTask: NodeTask,
) {

    private val log = LoggerFactory.getLogger(EntityGenerateService::class.java)

    fun exec(plantClassDiagram: PlantClassDiagram) {
        log.info("${plantClassDiagram.classes.size} classe(s) e ${plantClassDiagram.relationships.size} relacionamento(s) encontrados.")
        plantClassDiagram.classes.forEach { plantClass ->
            fsTask.copyFiles(
                classpathSource = "templates/entity",
                vars = mapOf(
                    "rootPackage" to env.getRequiredProperty("root.package"),
                    "entityPath" to env.getRequiredProperty("entity.path"),
                    "migrationPath" to env.getRequiredProperty("migration.path"),
                    "plantClass" to plantClass,
                    "plantRelationships" to plantClassDiagram.relationships.filter { rel -> rel.firstEntity.name == plantClass.name },
                    "vwName" to "VWLIST_${plantClass.name.uppercase().take(23)}",
                    "tableName" to plantClass.name.uppercase().take(30)
                )
            )
        }
        nodeTask.npx(listOf("""prettier --write "${env.getRequiredProperty("entity.path")}/*.java" >/dev/null """))
    }
}
