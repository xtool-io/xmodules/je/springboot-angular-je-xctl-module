package je.xctl.service

import org.springframework.core.env.Environment
import org.springframework.stereotype.Service
import xtool.core.context.WorkspaceContext
import xtool.core.tasks.FileSystemTask
import xtool.xdepng.tasks.NgTask
import xtool.xdepspringboot.model.Entity
import xtool.xdepspringboot.tasks.SpringBootTask

@Service
class NgFormCardGenerateService(
    private val fsTask: FileSystemTask,
    private val ngTask: NgTask,
    private val workspaceContext: WorkspaceContext,
    private val springBootTask: SpringBootTask,
    private val env: Environment,
) {

    fun exec(entity: Entity) {
        generateCard(entity)
        generateFormDataResolver(entity)
        generateDomain(entity)
        entity.toOneEntities
//            .filter { it.hasAnnotation("SettingEntity") }
            .forEach {
                generateFormDataResolver(it)
                generateDataSourceResolver(it)
                generateDomain(it)
            }
        entity.toManyEntities
//            .filter { it.hasAnnotation("SettingEntity") }
            .forEach {
                generateFormDataResolver(it)
                generateDataSourceResolver(it)
                generateDomain(it)
            }
    }

    private fun generateDomain(entity: Entity) {
        fsTask.copyFiles(
            classpathSource = "templates/ngdomain/formdata",
            vars = mapOf(
                "ngDomainPath" to env.getRequiredProperty("ng-domain.path"),
                "entity" to entity,
            )
        )
    }

    private fun generateFormDataResolver(entity: Entity) {
        fsTask.copyFiles(
            classpathSource = "templates/ngresolver/formdata",
            vars = mapOf(
                "ngResolverPath" to env.getRequiredProperty("ng-resolver.path"),
                "entity" to entity,
            )
        )
    }

    private fun generateDataSourceResolver(entity: Entity) {
        fsTask.copyFiles(
            classpathSource = "templates/ngresolver/datasource",
            vars = mapOf(
                "ngResolverPath" to env.getRequiredProperty("ng-resolver.path"),
                "entity" to entity,
            )
        )
    }

    private fun generateCard(entity: Entity) {
        val pageModule = ngTask.getProject().pageModules.asSequence().find { it.className == "${entity.name}PageModule" }!!
        fsTask.copyFiles(
            classpathSource = "templates/ngformcard",
            vars = mapOf(
                "ngPagePath" to workspaceContext.path.relativize(pageModule.path.parent),
                "ngDetailName" to entity.resourceName.plus("-detail"),
                "pageName" to pageModule.className,
                "entity" to entity,
            )
        )
    }
}
