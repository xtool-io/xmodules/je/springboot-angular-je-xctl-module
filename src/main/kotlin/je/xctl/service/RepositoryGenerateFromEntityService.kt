package je.xctl.service

import org.slf4j.LoggerFactory
import org.springframework.core.env.Environment
import org.springframework.stereotype.Service
import xtool.core.tasks.FileSystemTask
import xtool.xdepspringboot.model.Entity

@Service
class RepositoryGenerateFromEntityService(
    private val fsTask: FileSystemTask,
    private val env: Environment
) {

    private val log = LoggerFactory.getLogger(RepositoryGenerateFromEntityService::class.java)

    fun exec(entity: Entity) {
        generateRepository(entity)
        entity.toManyEntities
            .filter { it.hasAnnotation("SettingEntity") }
            .forEach { generateRepository(it) }
        entity.toOneEntities
            .filter { it.hasAnnotation("SettingEntity") }
            .forEach { generateRepository(it) }
    }

    private fun generateRepository(entity: Entity) {
        val vars = mapOf(
            "rootPackage" to env.getRequiredProperty("root.package"),
            "repositoryPath" to env.getRequiredProperty("repository.path"),
            "repositoryPackage" to env.getRequiredProperty("repository.package"),
            "entity" to entity,
        )
        log.debug("Gerando repositório a partir da entidade {}", entity.name)
        log.debug("vars: {}", vars)
        log.debug("classpathSource: templates/repository")
        fsTask.copyFiles(
            classpathSource = "templates/repository",
            vars = vars
        )
        log.debug("Cópia de arquivos da geração de repositório para entidade {} realizada com sucesso.", entity.name)
    }

}
