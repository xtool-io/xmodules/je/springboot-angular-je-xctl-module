package je.xctl.service

import org.springframework.core.env.Environment
import org.springframework.stereotype.Service
import xtool.core.context.WorkspaceContext
import xtool.core.tasks.FileSystemTask
import xtool.xdepng.model.NgPageModule
import xtool.xdepng.tasks.NgTask
import xtool.xdepspringboot.model.Entity

@Service
class NgFormDialogGenerateService(
    private val fsTask: FileSystemTask,
    private val ngTask: NgTask,
    private val workspaceContext: WorkspaceContext,
    private val env: Environment,
) {

    fun exec(entity: Entity) {
        val pageModule = ngTask.getProject().pageModules.asSequence().find { it.className == "${entity.name}PageModule" }!!
        generateFormDialog(pageModule, entity)
        generateResolver(entity)
        entity.toOneEntities
            .filter { it.hasAnnotation("SettingEntity") }
            .forEach {
            generateResolver(it)
        }
    }

    private fun generateResolver(entity: Entity) {
        fsTask.copyFiles(
            classpathSource = "templates/ngresolver/formdata",
            vars = mapOf(
                "ngResolverPath" to env.getRequiredProperty("ng-resolver.path"),
                "entity" to entity,
            )
        )
    }

    private fun generateFormDialog(pageModule: NgPageModule, entity: Entity) {
        fsTask.copyFiles(
            classpathSource = "templates/ngformdialog",
            vars = mapOf(
                "ngPagePath" to workspaceContext.path.relativize(pageModule.path.parent),
                "ngDetailName" to entity.resourceName.plus("-detail"),
                "pageName" to pageModule.className,
                "entity" to entity,
            )
        )
    }
}
