package je.xctl.service

import org.springframework.core.env.Environment
import org.springframework.stereotype.Service
import xtool.core.context.WorkspaceContext
import xtool.core.tasks.FileSystemTask
import xtool.xdepng.tasks.NgTask
import xtool.xdepspringboot.model.Entity
import xtool.xdepspringboot.tasks.SpringBootTask

@Service
class NgDataGridGenerateFromEntityService(
    private val fsTask: FileSystemTask,
    private val ngTask: NgTask,
    private val workspaceContext: WorkspaceContext,
    private val springBootTask: SpringBootTask,
    private val env: Environment,
) {

    fun exec(entity: Entity) {
        generateDataGrid(entity)
        generateResolver(entity)
        generateDomain(entity)
        entity.toManyEntities
            .filter { it.hasAnnotation("SettingEntity") }
            .forEach {
            generateResolver(it)
            generateDomain(it)
        }
        entity.toOneEntities
            .filter { it.hasAnnotation("SettingEntity") }
            .forEach {
            generateResolver(it)
            generateDomain(it)
        }
    }

    private fun generateDataGrid(entity: Entity) {
        val listViewEntity = springBootTask.listViewForEntity(entity)!!
        val pageModule = ngTask.getProject().pageModules.asSequence().find { it.className == "${entity.name}PageModule" }!!
        val classpathSource = when (entity.hasAnnotation("SettingEntity")) {
            true -> "templates/ngdatagrid/setting"
            else -> "templates/ngdatagrid/default"
        }
        fsTask.copyFiles(
            classpathSource = classpathSource,
            vars = mapOf(
                "ngPagePath" to workspaceContext.path.relativize(pageModule.path.parent),
                "ngListName" to entity.resourceName.plus("-list"),
                "pageName" to pageModule.className,
                "entity" to entity,
                "listViewEntity" to listViewEntity
            )
        )
    }

    private fun generateDomain(entity: Entity) {
        val listViewEntity = springBootTask.listViewForEntity(entity)!!
        fsTask.copyFiles(
            classpathSource = "templates/ngdomain/listview",
            vars = mapOf(
                "ngDomainPath" to env.getRequiredProperty("ng-domain.path"),
                "entity" to entity,
                "listViewEntity" to listViewEntity,
            )
        )
    }

    private fun generateResolver(entity: Entity) {
        fsTask.copyFiles(
            classpathSource = "templates/ngresolver/datasource",
            vars = mapOf(
                "ngResolverPath" to env.getRequiredProperty("ng-resolver.path"),
                "entity" to entity,
            )
        )
    }
}
