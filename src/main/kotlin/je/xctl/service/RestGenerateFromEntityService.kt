package je.xctl.service

import org.springframework.core.env.Environment
import org.springframework.stereotype.Service
import xtool.core.context.WorkspaceContext
import xtool.core.tasks.FileSystemTask
import xtool.xdepspringboot.model.Entity
import xtool.xdepspringboot.tasks.SpringBootTask

@Service
class RestGenerateFromEntityService(
    private val fsTask: FileSystemTask,
    private val springBootTask: SpringBootTask,
    private val env: Environment,
    private val workspaceContext: WorkspaceContext,
) {

    fun exec(entity: Entity) {
        generateRest(entity)
        generateRepository(entity)
        generateVwListRepository(entity)
        entity.toManyEntities
            .filter { it.hasAnnotation("SettingEntity") }
            .forEach {
            generateRest(it)
            generateRepository(it)
            generateVwListRepository(it)
        }
        entity.toOneEntities
            .filter { it.hasAnnotation("SettingEntity") }
            .forEach {
            generateRest(it)
            generateRepository(it)
            generateVwListRepository(it)
        }
    }

    private fun generateRest(entity: Entity) {
        fsTask.copyFiles(
            classpathSource = "templates/rest",
            vars = mapOf(
                "projectName" to workspaceContext.basename,
                "rootPackage" to env.getRequiredProperty("root.package"),
                "restPath" to env.getRequiredProperty("rest.path"),
                "entity" to entity,
            )
        )
    }

    private fun generateRepository(entity: Entity) {
        springBootTask.findEntityByName(entity.name)?.let {
            fsTask.copyFiles(
                classpathSource = "templates/repository",
                vars = mapOf(
                    "rootPackage" to env.getRequiredProperty("root.package"),
                    "repositoryPath" to env.getRequiredProperty("repository.path"),
                    "repositoryPackage" to env.getRequiredProperty("repository.package"),
                    "entity" to it,
                )
            )
        }
    }

    private fun generateVwListRepository(entity: Entity) {
        springBootTask.findEntityByName("${entity.name}ListView")?.let {
            fsTask.copyFiles(
                classpathSource = "templates/repository",
                vars = mapOf(
                    "rootPackage" to env.getRequiredProperty("root.package"),
                    "repositoryPath" to env.getRequiredProperty("repository.path").plus("/vw"),
                    "repositoryPackage" to env.getRequiredProperty("repository.package").plus(".vw"),
                    "entity" to it,
                )
            )
        }
    }
}
