package je.xctl.service

import org.springframework.core.env.Environment
import org.springframework.stereotype.Service
import xtool.core.tasks.FileSystemTask
import xtool.xdepspringboot.model.Entity

@Service
class NgServiceGenerateFromEntityService(
    private val fsTask: FileSystemTask,
    private val env: Environment,
    private val restGenerateFromEntityService: RestGenerateFromEntityService,
) {

    fun exec(entity: Entity) {
        generateRest(entity)
        generateNgService(entity)
        entity.toOneEntities.forEach {
            generateRest(it)
            generateNgService(it)
        }
        entity.toManyEntities.forEach {
            generateRest(it)
            generateNgService(it)
        }
    }

    private fun generateNgService(entity: Entity) {
        fsTask.copyFiles(
            classpathSource = "templates/ngservice",
            vars = mapOf(
                "ngServicePath" to env.getRequiredProperty("ng-service.path"),
                "entity" to entity,
            )
        )
    }

    private fun generateRest(entity: Entity) {
        restGenerateFromEntityService.exec(entity)
    }
}
