package je.xctl

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder

@SpringBootApplication
class SpringbootAngularJeXctlModuleApplication

fun main(args: Array<String>) {
    SpringApplicationBuilder(SpringbootAngularJeXctlModuleApplication::class.java)
        .logStartupInfo(false)
        .headless(false)
        .run(*args)
}
