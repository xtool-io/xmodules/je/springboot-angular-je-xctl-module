# springboot-angular-je-xctl-module

## Inicializando um módulo

**Passo 1.** Crie um diretório vazio:

```shell
$ mkdir <<NOME_DIRETORIO>>

$ cd <<NOME_DIRETORIO>>
```

**Passo 2.** Rodar os seguintes comandos:
```shell
$ bash <(curl -sSL "https://gitlab.com/xtool-io/xmodules/je/springboot-angular-je-xctl-module/-/raw/master/init.sh")
```

## Procedimentos para desenvolvimento local

**Passo 1.** Gerar o build da aplicação:

```shell
$ mvn clean install
```

**Passo 2.**. Rodar o módulo:

```shell
$ java -jar  $HOME/.m2/repository/je-xctl-module/springboot-angular-je-xctl-module/1.5.0/springboot-angular-je-xctl-module-1.5.0.jar
```

